#include "Player.h"

Player::Player()
{
	SetBaseTilesHeight(1);
	SetBaseTilesWidth(1);
	SetHitHeight(TILE_SIZE+TILE_SIZE/2);
	SetHitWidth(TILE_SIZE);
	frame_walk_delay = 4;
	frame_attack_delay = 2;
	frame_texture_tiles = 4;
	step_length = STEP_LENGTH;
	cooldown = 0;
}
Player::~Player(){}

void Player::DrawRect(int tex_id,float xo,float yo,float xf,float yf)
{
	int screen_x,screen_y;
	Rect hitbox = GetHitBox();
	int hitbox_w,hitbox_h;

	screen_x = GetX() + SCENE_Xo;
	screen_y = GetY() + SCENE_Yo;
	hitbox_w = hitbox.right - hitbox.left;
	hitbox_h = hitbox.top - hitbox.bottom;

	glEnable(GL_TEXTURE_2D);
	
	glBindTexture(GL_TEXTURE_2D,tex_id);
	glBegin(GL_QUADS);
		glTexCoord2f(xo,yo);	glVertex3i(screen_x - (TILE_SIZE*frame_texture_tiles - hitbox_w)/2,screen_y - (TILE_SIZE*frame_texture_tiles - hitbox_h)/2,screen_y);
		glTexCoord2f(xf,yo);	glVertex3i(screen_x + (TILE_SIZE*frame_texture_tiles + hitbox_w)/2,screen_y - (TILE_SIZE*frame_texture_tiles - hitbox_h)/2,screen_y);
		glTexCoord2f(xf,yf);	glVertex3i(screen_x + (TILE_SIZE*frame_texture_tiles + hitbox_w)/2,screen_y + (TILE_SIZE*frame_texture_tiles + hitbox_h)/2,screen_y);
		glTexCoord2f(xo,yf);	glVertex3i(screen_x - (TILE_SIZE*frame_texture_tiles - hitbox_w)/2,screen_y + (TILE_SIZE*frame_texture_tiles + hitbox_h)/2,screen_y);
	glEnd();

	glDisable(GL_TEXTURE_2D);
}
/* \brief ustawienie ramki na nast�pne kafelki*/
void Player::NextFrame(int max)
{
	SetDelay(GetDelay()+1);
	if(GetState() == STATE_ATTACKUP || GetState() == STATE_ATTACKRIGHT || GetState() == STATE_ATTACKDOWN || GetState() == STATE_ATTACKLEFT || GetState() == STATE_SUPERATTACK)
	{
		if(GetDelay() == frame_attack_delay)
		{
			SetFrame(GetFrame()+1);
			SetFrame(GetFrame()%max);
			SetDelay(0);
		}
	}
	else
	{
		if(GetDelay() == frame_walk_delay)
		{
			SetFrame(GetFrame()+1);
			SetFrame(GetFrame()%max);
			SetDelay(0);
		}
	}
}

int Player::GetFrameTop()
{
	return GetY() + GetHitHeight() + (frame_texture_tiles*TILE_SIZE - GetHitHeight())/2;
}
int Player::GetFrameBottom()
{
	return GetY() - (frame_texture_tiles*TILE_SIZE - GetHitHeight())/2;
}
int Player::GetFrameTexTiles()
{
	return frame_texture_tiles;
}

/*funkcja  sprawdza kolizje z wrogami*/
bool Player::CollidesWithEnemy(std::list<GObject*> &enemies,int next_tile)
{
	unsigned int i;
	std::list<GObject*>::iterator it;
	for(it=enemies.begin(); it!=enemies.end(); it++)
	{
		if((*it)->GetState() != STATE_DYING)
		{
			std::vector<int> tiles;
			tiles = (*it)->GetTiles();
			for(i=0; i<tiles.size(); i++) if(tiles[i] == next_tile) return true;
		}
	}
	return false;
}
/*Kolizja z gracza 102*/
bool Player::CollidesUD(bool *collision_map,std::list<GObject*> &enemies,bool up)
{	
	int tile_x,tile_y; //are the coordinates in tiles where the bug is after the step is taken
	int width_tiles; //liczba p�ytek w poziomie, z kt�rymi si� zderzy�

	tile_x = GetX() / TILE_SIZE;
	if(up && ((GetY() + GetBaseHeight()) / TILE_SIZE) < SCENE_HEIGHT) tile_y = (GetY() + GetBaseHeight()) / TILE_SIZE; //Sprawdzenie SCENE_HEIGHT ma na celu unikni�cie wykonywania oblicze� z pozycjami poza macierz� kolizji.
	else tile_y = GetY() / TILE_SIZE;

	width_tiles = GetBaseWidth()/TILE_SIZE;
	if((GetX() % TILE_SIZE) != 0) width_tiles++;

	//oceniaj kolizje z przeciwnikami, nie produkuj slajd�w
	for(int j=0;j<width_tiles;j++)
	{
		if(CollidesWithEnemy(enemies,(tile_x+j) + (tile_y*SCENE_WIDTH))) return true;
	}

	//oceniaj zderzenia z scen�, produkuj slajdy
	for(int j=1;j<(width_tiles-1);j++)
	{
		if(collision_map[ (tile_x+j) + (tile_y*SCENE_WIDTH) ]) return true;
	}
	// je�li wszystkie konsultowane p�ytki opr�cz ostatniej s� przeno�ne i mam mo�liwo�� skr�tu w lewo, wykonuj� slideL
	if(!collision_map[ (tile_x) + (tile_y*SCENE_WIDTH) ])
	{ 
		if(collision_map[ (tile_x+(width_tiles-1)) + (tile_y*SCENE_WIDTH) ]) 
		{
			if(GetState() != STATE_WALKLEFT && GetState() != STATE_WALKRIGHT && GetState() != STATE_SKILLWALKLEFT && GetState() != STATE_SKILLWALKRIGHT /*&& (x % TILE_SIZE) < TILE_SIZE/2*/) SetX(GetX()-step_length);
			return true;
		}
		return false;
	}
	// je�li wszystkie konsultowane p�ytki opr�cz pierwszej s� przeno�ne i mam mo�liwo�� skr�tu w prawo, to robi� slideR
	else
	{
		if(!collision_map[ (tile_x+(width_tiles-1)) + (tile_y*SCENE_WIDTH) ]) 
		{
			if(GetState() != STATE_WALKLEFT && GetState() != STATE_WALKRIGHT && GetState() != STATE_SKILLWALKLEFT && GetState() != STATE_SKILLWALKRIGHT /*&& (x % TILE_SIZE) >= TILE_SIZE/2*/) SetX(GetX()+step_length);
		}
		return true;
	}
}
bool Player::CollidesLR(bool *collision_map,std::list<GObject*> &enemies,bool right,bool vertical)
{
	int tile_x,tile_y; //are the coordinates in tiles where the bug is after the step is taken
	int height_tiles; //liczba p�ytek w pionie, z kt�rymi si� zderzy�

	if(right && ((GetX() + GetBaseWidth()) / TILE_SIZE) < SCENE_WIDTH) tile_x = (GetX() + GetBaseWidth()) / TILE_SIZE;
	else tile_x = GetX() / TILE_SIZE;
	tile_y = GetY() / TILE_SIZE;

	height_tiles = GetBaseHeight()/TILE_SIZE;
	if((GetY() % TILE_SIZE) != 0) height_tiles++;

	//oceniaj kolizje z przeciwnikami, nie produkuj slajd�w
	for(int j=0;j<height_tiles;j++)
	{
		if(CollidesWithEnemy(enemies,(tile_x) + ((tile_y+j)*SCENE_WIDTH))) return true;
	}

	//oceniaj kolizje z przeciwnikami, nie produkuj slajd�w
	for(int j=1;j<(height_tiles-1);j++)
	{
		return (collision_map[ (tile_x) + ((tile_y+j)*SCENE_WIDTH) ]);
	}
	// je�li wszystkie p�ytki poza najwy�ej konsultowanymi s� transferowalne, a ja mam potencja� zej�cia na d�, robi� slideD
	if(!collision_map[ (tile_x) + (tile_y*SCENE_WIDTH) ])
	{ 
		if(collision_map[ (tile_x) + ((tile_y+(height_tiles-1))*SCENE_WIDTH) ]) 
		{
			if(!vertical /*&& (y % TILE_SIZE) < TILE_SIZE/2*/) SetY(GetY()-step_length);
			return true;
		}
		return false;
	}
	// je�li wszystkie p�ytki poza najni�sz� konsultowan� s� zbywalne i mam potencja�, by p�j�� w g�r�, robi� slideU
	else
	{
		if(!collision_map[ (tile_x) + ((tile_y+(height_tiles-1))*SCENE_WIDTH) ]) 
		{
			if(!vertical /*&& (y % TILE_SIZE) >= TILE_SIZE/2*/) SetY(GetY()+step_length);
		}
		return true;
	}
}

void Player::RestrictedMoveUp(bool *collision_map,std::list<GObject*> &enemies,int h1,/*int h2,*/int game_height)
{
	MoveUp(collision_map,enemies);
}
void Player::RestrictedMoveDown(bool *collision_map,std::list<GObject*> &enemies,int h1,/*int h2,*/int game_height, int bottom_limit)
{
	if(h1 > bottom_limit) MoveDown(collision_map,enemies);
	else
	{
		if(GetState() != STATE_WALKLEFT && GetState() != STATE_WALKRIGHT && GetState() != STATE_SKILLWALKLEFT && GetState() != STATE_SKILLWALKRIGHT) 
		{
			if(GetState() != STATE_WALKDOWN && GetState() != STATE_SKILLWALKDOWN) {
				if(IsDoingSkill()) SetState(STATE_SKILLWALKDOWN);
				else SetState(STATE_WALKDOWN);
				SetFrame(0);
				SetDelay(0);
			}
		}
	}
}
void Player::MoveUp(bool *collision_map,std::list<GObject*> &enemies)
{
	int yaux;
	
	if(GetState() != STATE_WALKLEFT && GetState() != STATE_WALKRIGHT && GetState() != STATE_SKILLWALKLEFT && GetState() != STATE_SKILLWALKRIGHT) 
	{
		if(GetState() != STATE_WALKUP && GetState() != STATE_SKILLWALKUP) {
			if(IsDoingSkill()) SetState(STATE_SKILLWALKUP);
		else SetState(STATE_WALKUP);
			SetFrame(0);
			SetDelay(0);
		}
	}
	if( (GetY() % TILE_SIZE) == 0 )
	{
		yaux = GetY();
		SetY(GetY()+step_length);
		if(CollidesUD(collision_map,enemies,true)) SetY(yaux);
	}
	else SetY(GetY()+step_length);
}
void Player::MoveDown(bool *collision_map,std::list<GObject*> &enemies)
{
	int yaux;

	if(GetState() != STATE_WALKLEFT && GetState() != STATE_WALKRIGHT && GetState() != STATE_SKILLWALKLEFT && GetState() != STATE_SKILLWALKRIGHT) 
	{
		if(GetState() != STATE_WALKDOWN && GetState() != STATE_SKILLWALKDOWN) {
			if(IsDoingSkill()) SetState(STATE_SKILLWALKDOWN);
			else SetState(STATE_WALKDOWN);
			SetFrame(0);
			SetDelay(0);
		}
	}
	if( (GetY() % TILE_SIZE) == 0)
	{
		yaux = GetY();
		SetY(GetY()-step_length);
		if(CollidesUD(collision_map,enemies,false)) SetY(yaux);
	}
	else SetY(GetY()-step_length);
}
void Player::MoveLeft(bool *collision_map,std::list<GObject*> &enemies,bool vertical)
{
	int xaux;
	
	if(GetState() != STATE_WALKLEFT && GetState() != STATE_SKILLWALKLEFT) {
		if(IsDoingSkill()) SetState(STATE_SKILLWALKLEFT);
		else SetState(STATE_WALKLEFT);
		SetFrame(0);
		SetDelay(0);
	}
	if( (GetX() % TILE_SIZE) == 0)   // nast�pi zmiana p�ytki, je�li si� przeprowadz�
	{
		xaux = GetX();
		SetX(GetX()-step_length);
		if(CollidesLR(collision_map,enemies,false,vertical)) SetX(xaux);
	}
	else SetX(GetX()-step_length);
}
void Player::MoveRight(bool *collision_map,std::list<GObject*> &enemies,bool vertical)
{
	int xaux;

	if(GetState() != STATE_WALKRIGHT && GetState() != STATE_SKILLWALKRIGHT) {
		if(IsDoingSkill()) SetState(STATE_SKILLWALKRIGHT);
		else SetState(STATE_WALKRIGHT);
		SetFrame(0);
		SetDelay(0);
	}
	if( (GetX() % TILE_SIZE) == 0)
	{
		xaux = GetX();
		SetX(GetX()+step_length);

		if(CollidesLR(collision_map,enemies,true,vertical)) SetX(xaux);
	}
	else SetX(GetX()+step_length);
}

void Player::SetMaxSP(int max_sp)
{
	max_SP = max_sp;
}
int Player::GetMaxSP()
{
	return max_SP;
}
void Player::SetSP(int sp)
{
	SP = sp;
}
int Player::GetSP()
{
	return SP;
}
void Player::SetCharge(int c)
{
	charge = c;
}
int Player::GetCharge()
{
	return charge;
}
void Player::SetMaxPoints(int max_points)
{
	max_Points = max_points;
}
void Player::SetPoints(int points)
{
	pointss = points;
}
int  Player::GetMaxPoints()
{
	return max_Points;

}
int	 Player::GetPoints()
{
	return pointss;
}
void Player::SetFrameAttackDelay(int fad)
{
	frame_attack_delay = fad;
}
int Player::GetFrameAttackDelay()
{
	return frame_attack_delay;
}

void Player::SetLogicState(int state)
{
	logic_state = state;
}
int Player::GetLogicState()
{
	return logic_state;
}

void Player::StartSuperAttack()
{
	SetState(STATE_SUPERATTACK);
	
	SetFrame(0);
	SetDelay(0);
}
bool Player::IsAttacking()
{
	if( GetState() == STATE_ATTACKUP   || GetState() == STATE_ATTACKDOWN  ||
	    GetState() == STATE_ATTACKLEFT || GetState() == STATE_ATTACKRIGHT ||
		GetState() == STATE_SUPERATTACK ) return true;
	return false;
}
bool Player::IsSuperAttacking()
{
	if( GetState() == STATE_SUPERATTACK ) return true;
	return false;
}
bool Player::IsDoingSkill()
{
	if( GetState() == STATE_SKILLUP		  || GetState() == STATE_SKILLDOWN      ||
	    GetState() == STATE_SKILLLEFT     || GetState() == STATE_SKILLRIGHT     ||
		GetState() == STATE_SKILLWALKUP   || GetState() == STATE_SKILLWALKDOWN  ||
	    GetState() == STATE_SKILLWALKLEFT || GetState() == STATE_SKILLWALKRIGHT  ) return true;
	return false;
}
bool Player::IsWalking()
{
	if( GetState() == STATE_WALKUP        || GetState() == STATE_WALKDOWN       ||
	    GetState() == STATE_WALKLEFT      || GetState() == STATE_WALKRIGHT      ||
		GetState() == STATE_SKILLWALKUP   || GetState() == STATE_SKILLWALKDOWN  ||
	    GetState() == STATE_SKILLWALKLEFT || GetState() == STATE_SKILLWALKRIGHT  ) return true;
	return false;
}

void Player::TransformIntoSkillState()
{
	switch(GetState())
	{
		case STATE_LOOKUP:		SetState(STATE_SKILLUP);		break;
		case STATE_LOOKRIGHT:	SetState(STATE_SKILLRIGHT);		break;
		case STATE_LOOKDOWN:	SetState(STATE_SKILLDOWN);		break;
		case STATE_LOOKLEFT:	SetState(STATE_SKILLLEFT);		break;

		case STATE_WALKUP:		SetState(STATE_SKILLWALKUP);	break;
		case STATE_WALKRIGHT:	SetState(STATE_SKILLWALKRIGHT);	break;
		case STATE_WALKDOWN:	SetState(STATE_SKILLWALKDOWN);	break;
		case STATE_WALKLEFT:	SetState(STATE_SKILLWALKLEFT);	break;
	}
	SetFrame(0);
	SetDelay(0);
}

void Player::Stop()
{
	switch(GetState())
	{
		case STATE_WALKUP:			SetState(STATE_LOOKUP);		break;
		case STATE_WALKRIGHT:		SetState(STATE_LOOKRIGHT);	break;
		case STATE_WALKDOWN:		SetState(STATE_LOOKDOWN);	break;
		case STATE_WALKLEFT:		SetState(STATE_LOOKLEFT);	break;

		case STATE_SKILLWALKUP:		SetState(STATE_SKILLUP);	break;
		case STATE_SKILLWALKRIGHT:	SetState(STATE_SKILLRIGHT);	break;
		case STATE_SKILLWALKDOWN:	SetState(STATE_SKILLDOWN);	break;
		case STATE_SKILLWALKLEFT:	SetState(STATE_SKILLLEFT);	break;

		case STATE_ATTACKUP:		SetState(STATE_LOOKUP);		break;
		case STATE_ATTACKRIGHT:		SetState(STATE_LOOKRIGHT);	break;
		case STATE_ATTACKDOWN:		SetState(STATE_LOOKDOWN);	break;
		case STATE_ATTACKLEFT:		SetState(STATE_LOOKLEFT);	break;

		case STATE_DAMAGEUP:		SetState(STATE_LOOKUP);		break;
		case STATE_DAMAGERIGHT:		SetState(STATE_LOOKRIGHT);	break;
		case STATE_DAMAGEDOWN:		SetState(STATE_LOOKDOWN);	break;
		case STATE_DAMAGELEFT:		SetState(STATE_LOOKLEFT);	break;

		case STATE_SUPERATTACK:		SetState(STATE_LOOKDOWN);	break;
	}
	SetLogicState(GetState());
}

void Player::StopDoingSkill()
{
	switch(GetState())
	{
		case STATE_SKILLUP:			SetState(STATE_LOOKUP);		break;
		case STATE_SKILLRIGHT:		SetState(STATE_LOOKRIGHT);	break;
		case STATE_SKILLDOWN:		SetState(STATE_LOOKDOWN);	break;
		case STATE_SKILLLEFT:		SetState(STATE_LOOKLEFT);	break;

		case STATE_SKILLWALKUP:		SetState(STATE_LOOKUP);		break;
		case STATE_SKILLWALKRIGHT:	SetState(STATE_LOOKRIGHT);	break;
		case STATE_SKILLWALKDOWN:	SetState(STATE_LOOKDOWN);	break;
		case STATE_SKILLWALKLEFT:	SetState(STATE_LOOKLEFT);	break;
	}
	SetLogicState(GetState());
}

void Player::SetCooldown(int c)
{
	cooldown = c;
}
int Player::GetCooldown()
{
	return cooldown;
}

/*Rysowanie przeciwnik�w z kafelk�w*/
void Player::Draw(int tex_id, int tex_w, int tex_h, bool run)
{
	float xo, yo, xf, yf;
	bool reverse = false;
	float tex_offset_x, tex_offset_y;

	tex_offset_x = 1.0f / float(tex_w / (GetFrameTexTiles() * TEXTURE_TILE_SIZE));
	tex_offset_y = 1.0f / float(tex_h / (GetFrameTexTiles() * TEXTURE_TILE_SIZE));

	switch (GetState())
	{
	case STATE_LOOKUP:		xo = tex_offset_x * 1;   yo = tex_offset_y + tex_offset_y * 0;
		break;
	case STATE_LOOKRIGHT:	xo = tex_offset_x * 2;   yo = tex_offset_y + tex_offset_y * 0;
		break;
	case STATE_LOOKDOWN:	xo = tex_offset_x * 0;   yo = tex_offset_y + tex_offset_y * 0;
		break;
	case STATE_LOOKLEFT:	xo = tex_offset_x * 2;   yo = tex_offset_y + tex_offset_y * 0;
		reverse = true;
		break;

	case STATE_SKILLUP:		xo = tex_offset_x * 1;   yo = tex_offset_y + tex_offset_y * 9 + (GetFrame() * tex_offset_y);
		if (run) NextFrame(2);
		break;
	case STATE_SKILLRIGHT:	xo = tex_offset_x * 2;   yo = tex_offset_y + tex_offset_y * 9 + (GetFrame() * tex_offset_y);
		if (run) NextFrame(2);
		break;
	case STATE_SKILLDOWN:	xo = tex_offset_x * 0;   yo = tex_offset_y + tex_offset_y * 9 + (GetFrame() * tex_offset_y);
		if (run) NextFrame(2);
		break;
	case STATE_SKILLLEFT:	xo = tex_offset_x * 2;   yo = tex_offset_y + tex_offset_y * 9 + (GetFrame() * tex_offset_y);
		if (run) NextFrame(2);
		reverse = true;
		break;

	case STATE_WALKUP:		xo = tex_offset_x * 1;   yo = tex_offset_y + tex_offset_y * 1 + (GetFrame() * tex_offset_y);
		if (run) NextFrame(4);
		break;
	case STATE_WALKRIGHT:	xo = tex_offset_x * 2;   yo = tex_offset_y + tex_offset_y * 1 + (GetFrame() * tex_offset_y);
		if (run) NextFrame(4);
		break;
	case STATE_WALKDOWN:	xo = tex_offset_x * 0;   yo = tex_offset_y + tex_offset_y * 1 + (GetFrame() * tex_offset_y);
		if (run) NextFrame(4);
		break;
	case STATE_WALKLEFT:	xo = tex_offset_x * 2;   yo = tex_offset_y + tex_offset_y * 1 + (GetFrame() * tex_offset_y);
		if (run) NextFrame(4);
		reverse = true;
		break;

	case STATE_SKILLWALKUP:		xo = tex_offset_x * 1;   yo = tex_offset_y + tex_offset_y * 10 + (GetFrame() * tex_offset_y);
		if (run) NextFrame(2);
		break;
	case STATE_SKILLWALKRIGHT:	xo = tex_offset_x * 2;   yo = tex_offset_y + tex_offset_y * 10 + (GetFrame() * tex_offset_y);
		if (run) NextFrame(2);
		break;
	case STATE_SKILLWALKDOWN:	xo = tex_offset_x * 0;   yo = tex_offset_y + tex_offset_y * 10 + (GetFrame() * tex_offset_y);
		if (run) NextFrame(2);
		break;
	case STATE_SKILLWALKLEFT:	xo = tex_offset_x * 2;   yo = tex_offset_y + tex_offset_y * 10 + (GetFrame() * tex_offset_y);
		if (run) NextFrame(2);
		reverse = true;
		break;

	case STATE_DAMAGEUP:	xo = tex_offset_x * 1;   yo = tex_offset_y + tex_offset_y * 12 + (GetFrame() * tex_offset_y);
		if (run) NextFrame(2);
		break;
	case STATE_DAMAGERIGHT:	xo = tex_offset_x * 2;   yo = tex_offset_y + tex_offset_y * 12 + (GetFrame() * tex_offset_y);
		if (run) NextFrame(2);
		break;
	case STATE_DAMAGEDOWN:	xo = tex_offset_x * 0;   yo = tex_offset_y + tex_offset_y * 12 + (GetFrame() * tex_offset_y);
		if (run) NextFrame(2);
		break;
	case STATE_DAMAGELEFT:	xo = tex_offset_x * 2;   yo = tex_offset_y + tex_offset_y * 12 + (GetFrame() * tex_offset_y);
		if (run) NextFrame(2);
		reverse = true;
		break;

	case STATE_ATTACKUP:	xo = tex_offset_x * 1;   yo = tex_offset_y + tex_offset_y * 5 + (GetFrame() * tex_offset_y);
		if (run) NextFrame(4);
		break;
	case STATE_ATTACKRIGHT:	xo = tex_offset_x * 2;   yo = tex_offset_y + tex_offset_y * 5 + (GetFrame() * tex_offset_y);
		if (run) NextFrame(4);
		break;
	case STATE_ATTACKDOWN:	xo = tex_offset_x * 0;   yo = tex_offset_y + tex_offset_y * 5 + (GetFrame() * tex_offset_y);
		if (run) NextFrame(4);
		break;
	case STATE_ATTACKLEFT:	xo = tex_offset_x * 2;   yo = tex_offset_y + tex_offset_y * 5 + (GetFrame() * tex_offset_y);
		reverse = true;
		if (run) NextFrame(4);
		break;

	case STATE_SUPERATTACK:	xo = tex_offset_x * 3;   yo = tex_offset_y + tex_offset_y * 1 + (GetFrame() * tex_offset_y);
		if (run) NextFrame(changingStateSuperAttack);
		break;

	case STATE_DYING:		xo = tex_offset_x * 3;   yo = tex_offset_y + tex_offset_y * 0;
		break;
	}
	xf = xo + tex_offset_x;
	yf = yo - tex_offset_y;
	if (reverse)
	{
		float aux = xo;
		xo = xf;  xf = aux;
	}
	DrawRect(tex_id, xo, yo, xf, yf);
}