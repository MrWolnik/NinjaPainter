#include "Button.h"

Button::Button(double top, double left, double width, double height, void(*f)(void)) {
    t = top;
    l = left;
    w = width;
    h = height;
    onClick = f;
}

Button::~Button() {

}

void Button::CheckClick(double x, double y) {
    if (x >= l && x <= l + w && y >= t && y <= t + h) {
        ((void(*)())onClick)();
    }
}