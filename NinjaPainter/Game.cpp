#include "Game.h"
#include <iostream>
Game::Game(void)
{
	skipMusicInitialization = false;
	firstInitialization = true;
}

Game::~Game(void)
{
}

/* \brief Inicjalizacja wszystkich element�w gry takich jak: artefakty, muzyka, nak�adki, grafiki, mapy*/
bool Game::Init(int lvl)
{
	if (firstInitialization)
	{
		artefactCounter = new int[ARTEFACT_AMOUNT];
		for (int i = 0; i < ARTEFACT_AMOUNT; i++)
		{
			artefactCounter[i] = 0;

		}
		firstInitialization = false;
		player1 = new Player1();
	}
	else {
		//Player, wrogowie i inicjalizacja token�w
		LoadDynamicLayer(lvl);
	}

	if (artefactCounter[1] >= 5) {
		changePlayer = true;
	}

	bool res=true;
	int tex_w, tex_h;
	state = STATE_RUN;
	level = lvl;
	time = 1.0f;
	ia = true;
	epilepsia_mode = false;
	bonusStartTime = -1;
	//Inicjalizacja grafiki
	glClearColor(0.0f,0.0f,0.0f,0.0f);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	visible_area.left = TILE_SIZE;
	visible_area.right = GAME_WIDTH+TILE_SIZE;
	visible_area.bottom = TILE_SIZE;
	visible_area.top = GAME_HEIGHT+TILE_SIZE;
	//g��boko�� ka�dego obiektu jest okre�lona przez jego warto�� z podczas wywo�ania glVertex, pole widzenia b�dzie z=0(near) a SCENE_HEIGHT*TILE_SIZE(far)
	glOrtho(visible_area.left,visible_area.right,visible_area.bottom,visible_area.top,3,-SCENE_HEIGHT*TILE_SIZE-2-1); //ostatnie 2 warto�ci to znear i zfar, 3 jest dla pask�w/life overlays, interfejsu i menu, -2 jest dla terrain z, zakres: [-3,(SCENE_HEIGHT*TILE_SIZE)+2].
	glMatrixMode(GL_MODELVIEW);
	
	glAlphaFunc(GL_GREATER, 0.05f);
	glEnable(GL_ALPHA_TEST);
	glEnable(GL_DEPTH_TEST);

	// Inicjalizacja tekstury
	Data.Load();

	//Inicjalizacja terenu
	Data.GetSize(IMG_TILESET,&tex_w,&tex_h);
	res = Scene.LoadLevel(level,tex_w, tex_h);
	if(!res) return false;

	Scene.ComputeCollisionMaps();

	//Inicjalizacjia nak�adek
	Overlay1.SetBarOffsetX(TILE_SIZE*2);
	Overlay1.SetX(visible_area.left);
	Overlay1.SetY(visible_area.bottom);
	Overlay2.SetBarOffsetX(TILE_SIZE/2);
	Overlay2.SetX(visible_area.right - Overlay2.GetWidth());
	Overlay2.SetY(visible_area.bottom);
	Overlay3.SetX((visible_area.right + visible_area.left) / 2 - 75);
	Overlay3.SetY((visible_area.top + visible_area.bottom) / 2 - 50);
	Overlay4.SetX((visible_area.right + visible_area.left) / 2 - 75);
	Overlay4.SetY((visible_area.top + visible_area.bottom) / 2 - 50);

	//Inicjalizacja d�wi�ku
	Sound.Load();
	if (!skipMusicInitialization) {
		if (lvl == 1) Sound.Play(SOUND_AMBIENT1);
		if (lvl == 2) Sound.Play(SOUND_AMBIENT2);
	}

		skipMusicInitialization = false;

	return res;
}

/* \brief �adowanie dynamicznej mapy, w kt�rej znajduj� si� wrogowie i artefakty*/
bool Game::LoadDynamicLayer(int lvl)
{
	bool res;
	FILE *fd;
	char file[32];
	int i,j,id;
	char c;

	res=true;

	if(level<10) sprintf_s(file,"Levels/%s0%d%s",(char *)DYNAMIC_FILENAME,lvl,(char *)FILENAME_EXT);
	else		 sprintf_s(file,"Levels/%s%d%s",(char *)DYNAMIC_FILENAME,lvl,(char *)FILENAME_EXT);

	fopen_s(&fd, file,"r");
	if(fd==NULL) return false;

	for(j=SCENE_HEIGHT-1;j>=0;j--)
	{
		for(i=0;i<SCENE_WIDTH;i++)
		{
			fscanf_s(fd,"%i",&id);
			if(id == 1) player1->SetTile(i,j);
			else if(id == 3)
			{
				enemies.push_back(new Monster1);
				enemies.back()->SetTile(i,j);
			}
			else if(id == 4)
			{
				enemies.push_back(new Monster2);
				enemies.back()->SetTile(i,j);
			}
			else if(id == 5)
			{
				tokens.push_back(new cHeart);
				tokens.back()->SetTile(i,j);
			}
			else if (id == 6)
			{
				tokens.push_back(new Artefact1);
				tokens.back()->SetTile(i, j);
			}
			else if (id == 7)
			{
				tokens.push_back(new Artefact2);
				tokens.back()->SetTile(i, j);
			}
			else if (id == 8)
			{
				tokens.push_back(new Artefact3);
				tokens.back()->SetTile(i, j);
			}
			else if (id == 9)
			{
				tokens.push_back(new Artefact4);
				tokens.back()->SetTile(i, j);
			}
			else if (id == 10)
			{
				tokens.push_back(new Artefact5);
				tokens.back()->SetTile(i, j);
			}
			fscanf_s(fd,"%c",&c); //podaj przecinek lub enter
		}
	}

	fclose(fd);
	return res;
}

/* \brief G��wna p�tla gry*/
bool Game::Loop()
{
	bool res=true;
	int t1,t2;
	t1 = glutGet(GLUT_ELAPSED_TIME);
	if(state == STATE_RUN)
	{
		res = Process(t1);
		if(res) Render();
	}
	if(state == STATE_LEVELCHANGE)
	{
		Sound.StopAll();
		Sound.Play(SOUND_VICTORY);
		do { t2 = glutGet(GLUT_ELAPSED_TIME);
		if((t2-t1-3000) > 0) time = (float)(t2-t1-3000)/(float)(6000-3000);
		Render();
		} while(t2-t1 < 6000);

		std::list<GObject*>::iterator it_e;
		for(it_e=enemies.begin(); it_e!=enemies.end(); it_e++) delete (*it_e);
		enemies.clear();
		std::list<Bullet*>::iterator it_p;
		for(it_p=projectiles.begin(); it_p!=projectiles.end(); it_p++) delete (*it_p);
		projectiles.clear();
		std::list<Token*>::iterator it_t;
		for(it_t=tokens.begin(); it_t!=tokens.end(); it_t++) delete (*it_t);
		tokens.clear();

		Init(level+1);
		state = STATE_RUN;
	}
	if(state == STATE_GAMEOVER){
		Sound.StopAll();
		Sound.Play(SOUND_GAMEOVER);
		do { t2 = glutGet(GLUT_ELAPSED_TIME);
		time = (float)(t2-t1)/(float)3000;
		if(time > 1) time = 1.0f;
		if((t2-t1-7000) > 0) time += (float)(t2-t1-7000)/(float)(10700-7000);
		Render();
		} while(t2-t1 < 10700);
		res=false;
	}
	if(state == STATE_ENDGAME)
	{
		Sound.StopAll();
		Sound.Play(SOUND_VICTORY);
		do { t2 = glutGet(GLUT_ELAPSED_TIME);
		if((t2-t1-3000) > 0) time = (float)(t2-t1-3000)/(float)(6000-3000);
		Render();
		} while(t2-t1 < 6000);
		res=false;
	}
	

	do { t2 = glutGet(GLUT_ELAPSED_TIME);
	} while(t2-t1 < 1000/30);   //30 fps = 1000/30

	return res;
}

void Game::Finalize()
{
}

void Game::setTempData(int tab[],int points) {
	artefactCounter[0] = tab[0];
	artefactCounter[1] = tab[1];
	artefactCounter[2] = tab[2];
	artefactCounter[3] = tab[3];
	artefactCounter[4] = tab[4];
	player1->SetPoints(points);
}

//Input
void Game::ReadKeyboard(unsigned char key, int x, int y, bool press)
{
	if(key >='A' && key <= 'Z') key += 32;

	if (press && key == 'p') {
		std::ofstream levelFile;
		levelFile.open("level.txt");
		levelFile << level;
		levelFile.close();
		

		std::ofstream artefactFile;
		artefactFile.open("artefact.txt");
		for (int i = 0; i < ARTEFACT_AMOUNT; i++) {
			artefactFile << artefactCounter[i] << std::endl;
		}
		artefactFile.close();


		int pkt = player1->GetPoints();
		std::ofstream pointFile;
		pointFile.open("points.txt");
		pointFile << pkt;
		pointFile.close();

	}
	if (key == 'q') {
		exit(0);
	}

	//gdy boczny klawisz kierunkowy jest zwolniony podczas przewijania po przek�tnej, klatki gracza mog� si� zmienia�
	if(!press && ( key==P1_LEFT  || key==P1_RIGHT ) && !player1->IsAttacking()) player1->Stop();
	
	//gdy przestaniesz wciska� klawisz umiej�tno�ci, klatki gracza si� zmieni�
	if(!press && key==P1_SKILL) player1->StopDoingSkill();

	// atak jest rozpoznawany tylko wtedy, gdy klawisz ataku jest wci�ni�ty i zwolniony po wci�ni�ciu klawisza ataku
	if(!press && key == P1_ATTACK && keys[P1_ATTACK] && !keys[P1_SKILL]) p1_attacks=true;

	//w momencie naci�ni�cia klawisza umiej�tno�ci, obci��enie zostaje utracone
	if(press && key == P1_SKILL) player1->SetCharge(0);

	if(key != GLUT_KEY_UP && key != GLUT_KEY_DOWN && key != GLUT_KEY_LEFT && key != GLUT_KEY_RIGHT) keys[key] = press;
}

void Game::ReadSpecialKeyboard(unsigned char key, int x, int y, bool press)
{
	//gdy boczny klawisz kierunkowy jest zwolniony podczas przewijania po przek�tnej, klatki gracza mog� si� zmienia�.
	
	if(key == GLUT_KEY_UP || key == GLUT_KEY_DOWN || key == GLUT_KEY_LEFT || key == GLUT_KEY_RIGHT) keys[key] = press;
	if(key == GLUT_KEY_F1 && press) ia = !ia;
	if(key == GLUT_KEY_F10 && press) epilepsia_mode = !epilepsia_mode;
	if(key == GLUT_KEY_F10 && !press) time = 0.0f;
}

void Game::ReadMouse(int button, int state, int x, int y)
{
}

//Proces
bool Game::Process(int t1)
{
	Player1* dynamicPlayer1;
	Player2* dynamicPlayer2;
	if (bonusStartTime > 0 && bonusStartTime + BONUS_DURATION < t1){
		bonusStartTime = -1;
		ia = !ia;
	}
	bool res=true;
	bool stand=true;
	bool skill_p1=false;
	bool p1_defends=false;
	int h1;
	h1 = player1->GetY();

	//Process Input
	if(keys[27])	res=false;

	if(!player1->IsAttacking() && !player1->IsDamaged())
	{
		int input_state = player1->GetState();
		bool doing_skill = player1->IsDoingSkill();

		if(doing_skill) player1->SetState(player1->GetLogicState()); //gdy umiej�tno�� jest aktywna, stan pozostaje w sta�ym kierunku, wi�c zamiast atrybutu stanu, u�ywam logic_state z Player1 do obliczenia ruchu i po�lizgu
		if(keys[P1_UP])
		{
			player1->RestrictedMoveUp(Scene.GetCollisionMap(),enemies,h1/*,h2*/,GAME_HEIGHT); 
			stand=false;
		}
		else if(keys[P1_DOWN])
		{
			player1->RestrictedMoveDown(Scene.GetCollisionMap(),enemies,h1/*,h2*/,GAME_HEIGHT,visible_area.bottom);
			stand=false;
		}
		if(keys[P1_LEFT])
		{
			bool vertical = keys[P1_UP] || keys[P1_DOWN];
			player1->MoveLeft(Scene.GetCollisionMap(),enemies,vertical);
			stand=false;
		}
		else if(keys[P1_RIGHT])
		{
			bool vertical = keys[P1_UP] || keys[P1_DOWN];
			player1->MoveRight(Scene.GetCollisionMap(),enemies,vertical);
			stand=false;
		}
		player1->SetLogicState(player1->GetState());  //Po wykonaniu wszystkich mo�liwych ruch�w aktualizuj� stan logiczny Gracza na wypadek, gdyby trzeba by�o go u�y� w nast�pnej iteracji p�tli gry
		if(!doing_skill) input_state = player1->GetState(); //w tym przypadku imput_state nigdy nie b�dzie stanem umiej�tno�ci

		if(keys[P1_SKILL])
		{
			Player1* dynamicPlayer = dynamic_cast<Player1*>(player1);
			if (dynamicPlayer != NULL)
			{
				if (player1->GetSP() > 0) dynamicPlayer->SetShieldState(input_state);
				else player1->StopDoingSkill();
				p1_defends = true;
			}
			else {
				if (player1->GetSP() < player1->GetMaxSP()) {
					player1->SetSP(player1->GetSP()+1);
					if(!player1->IsDoingSkill()) player1->TransformIntoSkillState();
				}
				else player1->StopDoingSkill();
			}
		}
		else if(keys[P1_ATTACK])
		{
			player1->SetCharge(player1->GetCharge()+1); 
			if(player1->GetCharge() == CHARGE_BREAK) Sound.Play(SOUND_CHARGED);
		}
		if(stand)						 player1->Stop(); // je�li nic nie robi�, pokazuj� status LOOK

		stand=true;
	}

	//Game Logic

	std::list<Bullet*>::iterator it_p;
	std::list<GObject*>::iterator it_e;
	std::list<Token*>::iterator it_t;
	std::list<GObject*> players;
	players.push_back(player1);

	//czy�ci zu�yte strza�y, pokonanych wrog�w i uko�czone stany
	for(it_p=projectiles.begin(); it_p!=projectiles.end();)
	{
		if((*it_p)->IsEnding() && (*it_p)->GetFrame() == 0 && (*it_p)->GetDelay() == 0) 
		{
			delete(*it_p); // zwolnij pami�� dynamiczn� zaalokowan� przez new, kt�ry utworzy� pocisk
			it_p = projectiles.erase(it_p); //usu� wska�nik do pocisku i zaktualizuj iterator
		}
		else
		{
			
			Rect framebox = (*it_p)->GetFrameBox();
			if( framebox.left   >= visible_area.right - SCENE_Xo*2 || framebox.right <= visible_area.left ||
				framebox.bottom >= visible_area.top - SCENE_Yo*2   || framebox.top   <= visible_area.bottom ) // je�li opu�ci pole widzenia, wymazuj� go.
			{
				delete(*it_p); //zwolnij pami�� dynamiczn� zaalokowan� przez new, kt�ry utworzy� pocisk
				it_p = projectiles.erase(it_p); //usu� wska�nik do pocisku i zaktualizuj iterator
			}
			else it_p++;
		}
	}
	for(it_e=enemies.begin(); it_e!=enemies.end();)
	{
		if((*it_e)->GetState() == STATE_DYING && (*it_e)->GetFrame() == 0 && (*it_e)->GetDelay() == 0)
		{
			player1->SetPoints(player1->GetPoints() + 50);
			delete (*it_e); //zwolnij pami�� dynamiczn� zaalokowan� przez new, kt�ry utworzy� pocisk
			it_e = enemies.erase(it_e); //usu� wska�nik do pocisku i zaktualizuj iterator
		}
		else {
			if (dynamic_cast<Enemy*>(*it_e)->GetFrameTop() < visible_area.bottom) // je�li opu�ci pole widzenia, wymazuj� go
			{
				delete (*it_e); //zwolnij pami�� dynamiczn� zaalokowan� przez new, kt�ry utworzy� pocisk
				it_e = enemies.erase(it_e); //usu� wska�nik do pocisku i zaktualizuj iterator
			}
			else
			{
				if(((*it_e)->IsAttacking() || (*it_e)->IsDamaged()) && (*it_e)->GetFrame() == 0 && (*it_e)->GetDelay() == 0) (*it_e)->Stop();
				if((*it_e)->IsWalking() && (*it_e)->GetX()%TILE_SIZE == 0 && (*it_e)->GetY()%TILE_SIZE == 0) (*it_e)->Stop();
				it_e++;
			}
		}
	}
	if((player1->IsAttacking() || player1->IsDamaged()) && player1->GetFrame() == 0 && player1->GetDelay() == 0) player1->Stop();


	dynamicPlayer1 = dynamic_cast<Player1*>(player1);
	dynamicPlayer2 = dynamic_cast<Player2*>(player1);

	if (dynamicPlayer1 != NULL)
	{
		if (!dynamicPlayer1->IsAttacking() && !dynamicPlayer1->IsDamaged() && p1_attacks && dynamicPlayer1->GetCooldown() == 0)
		{
			if (dynamicPlayer1->GetCharge() >= CHARGE_BREAK)
			{
				if (dynamicPlayer1->GetSP() >= SUPERATTACK_COST)
				{

					dynamicPlayer1->StartSuperAttack();
					dynamicPlayer1->SetSP(dynamicPlayer1->GetSP() - SUPERATTACK_COST);
					Sound.Play(SOUND_SUPERSLASH);
				}
				else Sound.Play(SOUND_FAIL);
			}
			else
			{
				if (dynamicPlayer1->GetSP() >= ATTACK_COST)
				{
					dynamicPlayer1->StartAttack();
					dynamicPlayer1->SetSP(dynamicPlayer1->GetSP() - ATTACK_COST);
					Sound.Play(SOUND_SLASH);
				}
				else Sound.Play(SOUND_FAIL);
			}
			dynamicPlayer1->SetCharge(0);
			p1_attacks = false;
			dynamicPlayer1->SetCooldown(COOLDOWN);
		}
	}else{
		if(!dynamicPlayer2->IsAttacking()  && !dynamicPlayer2->IsDamaged() && p1_attacks && dynamicPlayer2->GetCooldown() == 0)
		{
			if(dynamicPlayer2->GetCharge() >= CHARGE_BREAK)
			{
				if(dynamicPlayer2->GetSP() >= ATTACK_COST)
				{
					dynamicPlayer2->StartSuperAttack();
					Sound.Play(SOUND_SUPERFIREBALL);
				}
				else Sound.Play(SOUND_FAIL);
			}
			else
			{
				if(dynamicPlayer2->GetSP() >= ATTACK_COST)
				{
					dynamicPlayer2->StartAttack();
					Sound.Play(SOUND_FIREBALL);
				}
				else Sound.Play(SOUND_FAIL);
			}
			dynamicPlayer2->SetCharge(0);
			p1_attacks=false;
			dynamicPlayer2->SetCooldown(COOLDOWN);
		}
	}


	dynamicPlayer1 = dynamic_cast<Player1*>(player1);
	dynamicPlayer2 = dynamic_cast<Player2*>(player1);
	if (dynamicPlayer1 != NULL)
	{
		if (!dynamicPlayer1->IsAttacking())
		{
			if (dynamicPlayer1->GetCooldown() > 0) dynamicPlayer1->SetCooldown(dynamicPlayer1->GetCooldown() - 1);
			if (!dynamicPlayer1->IsDamaged() && !p1_defends && dynamicPlayer1->GetCharge() == 0 && dynamicPlayer1->GetSP() < dynamicPlayer1->GetMaxSP()) dynamicPlayer1->SetSP(dynamicPlayer1->GetSP() + 1);
		}
		else
		{
			std::list<GObject*>::iterator it;
			for (it = enemies.begin(); it != enemies.end(); it++)
			{

				if (dynamicPlayer1->Attack(*it))
				{
					Sound.Play(SOUND_HIT);
					if ((*it)->GetState() == STATE_DYING) Sound.Play(SOUND_DEATH);
				}
			}
		}
	}
	else {
		if(!dynamicPlayer2->IsAttacking())
		{
			if(dynamicPlayer2->GetCooldown() > 0) dynamicPlayer2->SetCooldown(dynamicPlayer2->GetCooldown()-1);
		}
		else
		{
			if( dynamicPlayer2->Attack(enemies,projectiles,Scene.GetProjCollisionMap()) )
			{
				if( projectiles.back()->IsEnding() ) Sound.Play(SOUND_EXPLOSION);
			}
		}
	}

	for(it_e=enemies.begin(); it_e!=enemies.end(); it_e++) 
	{
		if(ia)
		{
			if(dynamic_cast<Enemy*>(*it_e)->GetFrameBottom() <= visible_area.top)
			{
				if((*it_e)->IsWalking()) dynamic_cast<Enemy*>(*it_e)->Move();
				else if((*it_e)->IsAttacking())
				{
					if(dynamic_cast<Monster1*>(*it_e) != NULL) //sprawdzamy do jakiej podklasy nale�y wska�nik
					{
						std::list<GObject*>::iterator it;
						for(it=players.begin(); it!=players.end(); it++)
						{
							if( dynamic_cast<Monster1*>(*it_e)->Attack(*it) ) Sound.Play(SOUND_HIT);
						}
					}
					if(dynamic_cast<Monster2*>(*it_e) != NULL) //sprawdzamy do jakiej podklasy nale�y wska�nik
					{
						dynamic_cast<Monster2*>(*it_e)->Attack(players,projectiles,Scene.GetProjCollisionMap());
					}
				}
				else if((*it_e)->IsLooking() && (*it_e)->GetY() <= visible_area.top) //AI dzia�a tylko wtedy, gdy nie ma ju� przypisanych �adnych zada�
				{
					if(dynamic_cast<Monster1*>(*it_e) != NULL) //sprawdzamy do jakiej podklasy nale�y kasownik
					{
						if( dynamic_cast<Monster1*>(*it_e)->IA(Scene.GetCollisionMap(),enemies,player1/*,&Player2*/) ) Sound.Play(SOUND_SLASH);
					}
					if(dynamic_cast<Monster2*>(*it_e) != NULL) 
					{
						if( dynamic_cast<Monster2*>(*it_e)->IA(Scene.GetCollisionMap(),Scene.GetProjCollisionMap(),enemies,player1/*&Player2*/) ) Sound.Play(SOUND_ARROW);
					}
				}
			}
		}
		else if(!(*it_e)->IsDamaged()) (*it_e)->Stop();
	}

	for(it_p=projectiles.begin(); it_p!=projectiles.end(); it_p++)
	{
		if(dynamic_cast<Fireball*>(*it_p) != NULL) 
		{
			if( (*it_p)->Logic(Scene.GetProjCollisionMap(),enemies) ) Sound.Play(SOUND_EXPLOSION);
		}
		if(dynamic_cast<Arrow*>(*it_p) != NULL)
		{
			(*it_p)->Logic(Scene.GetProjCollisionMap(),players);
		}
	}

	for(it_t=tokens.begin(); it_t!=tokens.end();)
	{
		bool used=false;
		if( player1->Intersection(player1->GetHitBox(),(*it_t)->GetHitBox()) )
		{
			if(dynamic_cast<cHeart*>(*it_t) != NULL) //sprawdzamy do jakiej podklasy nale�y wska�nik
			{
				used = dynamic_cast<cHeart*>(*it_t)->Effect(*player1);
			}
		}
		if (player1->Intersection(player1->GetHitBox(), (*it_t)->GetHitBox()))
		{
			if (dynamic_cast<Artefact1*>(*it_t) != NULL) //sprawdzamy do jakiej podklasy nale�y wska�nik
			{
				used = dynamic_cast<Artefact1*>(*it_t)->Effect(*player1);
				artefactCounter[0]++;
				if (artefactCounter[0] == 5) {
					changePlayer = true;
				}
			}
		}
		if (player1->Intersection(player1->GetHitBox(), (*it_t)->GetHitBox()))
		{
			if (dynamic_cast<Artefact2*>(*it_t) != NULL) //sprawdzamy do jakiej podklasy nale�y wska�nik
			{
				used = dynamic_cast<Artefact2*>(*it_t)->Effect(*player1);
				artefactCounter[1]++;
			}
		}
		if (player1->Intersection(player1->GetHitBox(), (*it_t)->GetHitBox()))
		{
			if (dynamic_cast<Artefact3*>(*it_t) != NULL) //sprawdzamy do jakiej podklasy nale�y wska�nik
			{
				used = dynamic_cast<Artefact3*>(*it_t)->Effect(*player1);
				artefactCounter[2]++;
			}
		}
		if (player1->Intersection(player1->GetHitBox(), (*it_t)->GetHitBox()))
		{
			if (dynamic_cast<Artefact4*>(*it_t) != NULL) //sprawdzamy do jakiej podklasy nale�y wska�nik
			{
				used = dynamic_cast<Artefact4*>(*it_t)->Effect(*player1);
					artefactCounter[3]++;
			}
		}
		if (player1->Intersection(player1->GetHitBox(), (*it_t)->GetHitBox()))
		{
			if (dynamic_cast<Artefact5*>(*it_t) != NULL) //sprawdzamy do jakiej podklasy nale�y wska�nik
			{
				used = dynamic_cast<Artefact5*>(*it_t)->Effect(*player1);
					ia = !ia;
					bonusStartTime = t1;
					artefactCounter[4]++;
			}
		}
		if(used)
		{
			delete (*it_t);
			it_t = tokens.erase(it_t);
			Sound.Play(SOUND_TOKEN);
		}
		else it_t++;
		if (changePlayer)
		{
			changePlayer = false;
			Player2* tempPlayer = new Player2();
			tempPlayer->SetState(player1->GetState());
			tempPlayer->SetY(player1->GetY());
			tempPlayer->SetX(player1->GetX());
			tempPlayer->SetPoints(player1->GetPoints());
			free(player1);
			player1 = tempPlayer;
		}
	}



	//Updates
	if (player1->GetState() == STATE_DYING) {
		state = STATE_GAMEOVER;
		hasPlayerLose = true;
	}
	else if(player1->GetY() >= visible_area.top - TILE_SIZE )
	{
		if(level < TOTAL_LEVELS) state = STATE_LEVELCHANGE;
		else {
			state = STATE_ENDGAME;
			hasPlayerWon = true;
		}
	}
	else {
		UpdateCamera(h1);
		Sound.Update();
	}

	return res;
}

void Game::UpdateCamera(int h1)
{
	int mean;
	mean = h1 + SCENE_Yo;

	if(mean > (GAME_HEIGHT/2) && mean < (SCENE_HEIGHT*TILE_SIZE - (GAME_HEIGHT-SCENE_Yo*4)/2) - TILE_SIZE*2 && mean - GAME_HEIGHT/2 + TILE_SIZE > visible_area.bottom) {
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		visible_area.bottom = mean - GAME_HEIGHT/2;
		visible_area.top = mean + GAME_HEIGHT/2;
		glOrtho(visible_area.left,visible_area.right,visible_area.bottom,visible_area.top,3,-SCENE_HEIGHT*TILE_SIZE-2-1);
		Overlay1.SetY(visible_area.bottom);   //Nak�adki poruszaj� si� wraz z kamer�
		Overlay2.SetY(visible_area.bottom);
		Overlay3.SetY(visible_area.bottom);
		Overlay4.SetY(visible_area.bottom);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
	}
}

void Game::Reshape(int w,int h) //w i h to wymiary okna gry
{
	//trzeba zmodyfikowa� game_height i width
	glViewport(0,0,w,h);
}

//Wyj�cie
void Game::Render()
{
	int tex_w,tex_h;
	int tex_w2,tex_h2;
	bool run = (state == STATE_RUN);
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	Overlay2.drawArtefactAmmount(artefactCounter, ARTEFACT_AMOUNT);
	Overlay2.drawPoints(player1->GetPoints());

	//Malowanie
	Data.GetSize(IMG_TILESET,&tex_w,&tex_h);
	Scene.Draw(Data.GetID(IMG_TILESET),tex_w,tex_h,run);
	int choosenHero;
	if (dynamic_cast<Player1*>(player1) != NULL)
	{
		choosenHero = IMG_PLAYER1;
	}
	else {
		choosenHero = IMG_PLAYER2;
	}
	Data.GetSize(choosenHero,&tex_w,&tex_h);
	if(player1->GetCharge() >= CHARGE_BREAK && player1->GetCharge()%4 < 2) player1->Draw(Data.GetID(IMG_SUPERP1),tex_w,tex_h,run);
	else player1->Draw(Data.GetID(choosenHero),tex_w,tex_h,run);

	Data.GetSize(IMG_MONSTER1,&tex_w,&tex_h);
	Data.GetSize(IMG_MONSTER2,&tex_w2,&tex_h2);
	std::list<GObject*>::iterator it_e;
	for(it_e=enemies.begin(); it_e!=enemies.end(); it_e++) 
	{
		if(dynamic_cast<Monster1*>(*it_e) != NULL) //sprawdzamy do jakiej podklasy nale�y wska�nik
		{
			(*it_e)->Draw(Data.GetID(IMG_MONSTER1),tex_w,tex_h,run);
		}
		if(dynamic_cast<Monster2*>(*it_e) != NULL) //sprawdzamy do jakiej podklasy nale�y wska�nik
		{
			(*it_e)->Draw(Data.GetID(IMG_MONSTER2),tex_w2,tex_h2,run);
		}
	}

	Data.GetSize(IMG_HEART,&tex_w,&tex_h);
	std::list<Token*>::iterator it_t;
	for(it_t=tokens.begin(); it_t!=tokens.end(); it_t++) 
	{
		if(dynamic_cast<cHeart*>(*it_t) != NULL) //sprawdzamy do jakiej podklasy nale�y wska�nik
		{
			(*it_t)->Draw(Data.GetID(IMG_HEART),tex_w,tex_h,run);
		}
	}

	Data.GetSize(IMG_ARTEFACT1, &tex_w, &tex_h);
	for (it_t = tokens.begin(); it_t != tokens.end(); it_t++)
	{
		if (dynamic_cast<Artefact1*>(*it_t) != NULL) //sprawdzamy do jakiej podklasy nale�y wska�nik
		{
			(*it_t)->Draw(Data.GetID(IMG_ARTEFACT1), tex_w, tex_h, run);
		}
	}
	Data.GetSize(IMG_ARTEFACT2, &tex_w, &tex_h);
	for (it_t = tokens.begin(); it_t != tokens.end(); it_t++)
	{
		if (dynamic_cast<Artefact2*>(*it_t) != NULL) //sprawdzamy do jakiej podklasy nale�y wska�nik
		{
			(*it_t)->Draw(Data.GetID(IMG_ARTEFACT2), tex_w, tex_h, run);
		}
	}
	Data.GetSize(IMG_ARTEFACT3, &tex_w, &tex_h);
	for (it_t = tokens.begin(); it_t != tokens.end(); it_t++)
	{
		if (dynamic_cast<Artefact3*>(*it_t) != NULL) //sprawdzamy do jakiej podklasy nale�y wska�nik
		{
			(*it_t)->Draw(Data.GetID(IMG_ARTEFACT3), tex_w, tex_h, run);
		}
	}
	Data.GetSize(IMG_ARTEFACT4, &tex_w, &tex_h);
	for (it_t = tokens.begin(); it_t != tokens.end(); it_t++)
	{
		if (dynamic_cast<Artefact4*>(*it_t) != NULL) //sprawdzamy do jakiej podklasy nale�y wska�nik
		{
			(*it_t)->Draw(Data.GetID(IMG_ARTEFACT4), tex_w, tex_h, run);
		}
	}
	Data.GetSize(IMG_ARTEFACT5, &tex_w, &tex_h);
	for (it_t = tokens.begin(); it_t != tokens.end(); it_t++)
	{
		if (dynamic_cast<Artefact5*>(*it_t) != NULL) //sprawdzamy do jakiej podklasy nale�y wska�nik
		{
			(*it_t)->Draw(Data.GetID(IMG_ARTEFACT5), tex_w, tex_h, run);
		}
	}

	Data.GetSize(IMG_FIREBALL,&tex_w,&tex_h);
	Data.GetSize(IMG_ARROW,&tex_w2,&tex_h2);
	std::list<Bullet*>::reverse_iterator it_p;
	glEnable(GL_BLEND);			   // Turn Blending On
	//glDisable(GL_DEPTH_TEST);    // Turn Depth Testing Off
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	for(it_p=projectiles.rbegin(); it_p!=projectiles.rend(); it_p++) //powtarzamy to w odwrotnej kolejno�ci, tak aby ostatnie utworzone kule ognia by�y namalowane na poprzednich, dzi�ki czemu zobaczymy prawid�owe warto�ci alfa
	{
		if(dynamic_cast<Fireball*>(*it_p) != NULL) 
		{
			(*it_p)->Draw(Data.GetID(IMG_FIREBALL),tex_w,tex_h,run);
		}
		if(dynamic_cast<Arrow*>(*it_p) != NULL) //sprawdzamy do jakiej podklasy nale�y wska�nik
		{
			(*it_p)->Draw(Data.GetID(IMG_ARROW),tex_w2,tex_h2,run);
		}
	}
	
	Overlay1.DrawBars(player1->GetMaxHealth(),player1->GetHealth(),player1->GetMaxSP(),player1->GetSP());

	Overlay1.Draw(Data.GetID(IMG_OVERLAY1));
	Overlay2.Draw(Data.GetID(IMG_OVERLAY2));
	if (hasPlayerWon)
	{
		Overlay4.Draw(Data.GetID(IMG_WIN));
	}
	if (hasPlayerLose)
	{
		Overlay3.Draw(Data.GetID(IMG_GAMEOVER));
	}
	glDisable(GL_BLEND);          // Wy��czanie mieszania
    //glEnable(GL_DEPTH_TEST);    // W��cz testowanie g��boko�cin
	glAlphaFunc(GL_GREATER, 0.05f);

	glutSwapBuffers();
}
