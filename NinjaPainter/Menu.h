#pragma once

#include "Overlay.h"
#include "Data.h"
#include "Sound.h"
#include <vector>
#include "button.h"

class Menu {

public:
	Menu(void);

	void ReadMouse(int button, int state, int x, int y);
	void AddButton(Button b);

	void Render();

	void DrawMenu();

	bool drawButtons();

private:
	std::vector<Button> buttons;

};
