#include "Monster1.h"

Monster1::Monster1()
{
	SetBaseTilesHeight(1);
	SetBaseTilesWidth(1);
	SetHitHeight(TILE_SIZE+TILE_SIZE/2);
	SetHitWidth(TILE_SIZE);
	SetFrameTexTiles(4);
	SetMaxHealth(30);
	SetHealth(30);
	SetDamage(10);
}
Monster1::~Monster1(){}


/* \brief funkcja Attack - atakowanie gracza przez przeciwnika */
bool Monster1::Attack(GObject* player)
{
	bool hit = false;
	if(GetFrame() == 3 && GetDelay() == 0)
	{
		if(GetState() == STATE_ATTACKUP)
		{
			Rect attack_area;
			attack_area.top = GetY()+GetBaseHeight()+TILE_SIZE;
			attack_area.bottom = GetY()+GetBaseHeight();
			attack_area.left = GetX();
			attack_area.right = GetX()+GetBaseWidth();

			if( player->GetState() != STATE_DYING && Intersection(attack_area,player->GetHitBox()) ) 
			{
				Player1* player1 = dynamic_cast<Player1*>(player);
				if(player1 != NULL && player1->IsDoingSkill()) player1->TestShieldProtection(STATE_ATTACKUP,GetDamage(),GetX(),GetY());
				else
				{
					int new_health = player->GetHealth() - GetDamage();
					if(new_health <= 0) player->SetState(STATE_DYING);
					else if(!dynamic_cast<Player*>(player)->IsSuperAttacking()) 
					{
						player->SetState(STATE_DAMAGEDOWN);
						player->SetFrame(0);
						player->SetDelay(0);
					}
					if(new_health < 0) new_health = 0;
					player->SetHealth(new_health);
					hit = true;
				}
			}
		}
		else if(GetState() == STATE_ATTACKDOWN)
		{
			Rect attack_area;
			attack_area.top = GetY();
			attack_area.bottom = GetY()-TILE_SIZE;
			attack_area.left = GetX();
			attack_area.right = GetX()+GetBaseWidth();

			if( player->GetState() != STATE_DYING && Intersection(attack_area,player->GetHitBox()) ) 
			{
				Player1* player1 = dynamic_cast<Player1*>(player);
				if(player1 != NULL && player1->IsDoingSkill()) player1->TestShieldProtection(STATE_ATTACKDOWN,GetDamage(),GetX(),GetY());
				else
				{
					int new_health = player->GetHealth() - GetDamage();
					if(new_health <= 0) player->SetState(STATE_DYING);
					else if(!dynamic_cast<Player*>(player)->IsSuperAttacking()) 
					{
						player->SetState(STATE_DAMAGEUP);
						player->SetFrame(0);
						player->SetDelay(0);
					}
					if(new_health < 0) new_health = 0;
					player->SetHealth(new_health);
					hit = true;
				}
			}
		}
		else if(GetState() == STATE_ATTACKLEFT)
		{
			Rect attack_area;
			attack_area.top = GetY()+GetBaseHeight();
			attack_area.bottom = GetY();
			attack_area.left = GetX()-TILE_SIZE;
			attack_area.right = GetX();

			if( player->GetState() != STATE_DYING && Intersection(attack_area,player->GetHitBox()) ) 
			{
				Player1* player1 = dynamic_cast<Player1*>(player);
				if(player1 != NULL && player1->IsDoingSkill()) player1->TestShieldProtection(STATE_ATTACKLEFT,GetDamage(),GetX(),GetY());
				else
				{
					int new_health = player->GetHealth() - GetDamage();
					if(new_health <= 0) player->SetState(STATE_DYING);
					else if(!dynamic_cast<Player*>(player)->IsSuperAttacking()) 
					{
						player->SetState(STATE_DAMAGERIGHT);
						player->SetFrame(0);
						player->SetDelay(0);
					}
					if(new_health < 0) new_health = 0;
					player->SetHealth(new_health);
					hit = true;
				}
			}
		}
		else if(GetState() == STATE_ATTACKRIGHT)
		{
			Rect attack_area;
			attack_area.top = GetY()+GetBaseHeight();
			attack_area.bottom = GetY();
			attack_area.left = GetX()+GetBaseWidth();
			attack_area.right = GetX()+GetBaseWidth()+TILE_SIZE;

			if( player->GetState() != STATE_DYING && Intersection(attack_area,player->GetHitBox()) ) 
			{
				Player1* player1 = dynamic_cast<Player1*>(player);
				if(player1 != NULL && player1->IsDoingSkill()) player1->TestShieldProtection(STATE_ATTACKRIGHT,GetDamage(),GetX(),GetY());
				else
				{
					int new_health = player->GetHealth() - GetDamage();
					if(new_health <= 0) player->SetState(STATE_DYING);
					else if(!dynamic_cast<Player*>(player)->IsSuperAttacking()) 
					{
						player->SetState(STATE_DAMAGELEFT);
						player->SetFrame(0);
						player->SetDelay(0);
					}
					if(new_health < 0) new_health = 0;
					player->SetHealth(new_health);
					hit = true;
				}
			}
		}
	}
	return hit;
}

/* \brief funkcja atakowania - sprawdzenie czy gracz jest osiagalny i mo�liwy jest atak */
bool Monster1::IA(bool *collision_map,std::list<GObject*> &enemies,GObject* p1)
{
	unsigned int i;
	// je�li gracz jest osi�galny, przypisuj� mu odpowiedni status ataku
	Rect hitbox_p1 = p1->GetHitBox();

	Rect attack_areaV,attack_areaH;

	attack_areaV.top = GetY()+GetBaseHeight()+TILE_SIZE;
	attack_areaV.bottom = GetY()-TILE_SIZE;
	attack_areaV.left = GetX();
	attack_areaV.right = GetX()+GetBaseWidth();

	attack_areaH.top = GetY()+GetBaseHeight();
	attack_areaH.bottom = GetY();
	attack_areaH.left = GetX()-TILE_SIZE;
	attack_areaH.right = GetX()+GetBaseWidth()+TILE_SIZE;

	if(Intersection(attack_areaV,hitbox_p1))
	{
		if(hitbox_p1.bottom < GetY())   SetState(STATE_ATTACKDOWN);
		else SetState(STATE_ATTACKUP);
		SetFrame(0);
		SetDelay(0);
		return true;
	}
	else if(Intersection(attack_areaH,hitbox_p1))
	{
		if(hitbox_p1.left < GetX())   SetState(STATE_ATTACKLEFT);
		else SetState(STATE_ATTACKRIGHT);
		SetFrame(0);
		SetDelay(0);
		return true;
	}

	//Je�li nie mo�e zaatakowa�, obliczam �cie�k� i przypisuj� odpowiedni stan ruchu.
	//Je�li zosta� zaatakowany podczas ruchu, m�g� zosta� przesuni�ty poza kraw�dzie p�ytki, nale�y go ponownie umie�ci�
	if(GetX()%TILE_SIZE != 0)
	{
		if(GetX()%TILE_SIZE < TILE_SIZE/2) SetState(STATE_WALKLEFT);
		else SetState(STATE_WALKRIGHT);
	}
	else if(GetY()%TILE_SIZE != 0)
	{
		if(GetY()%TILE_SIZE < TILE_SIZE/2) SetState(STATE_WALKDOWN);
		else SetState(STATE_WALKUP);
	}
	else
	{
		int new_collision_map[SCENE_WIDTH * SCENE_HEIGHT]; //0 dla pustych, 1 dla przeszk�d, 2 dla gracza
		std::vector<int> tiles;
		std::list<GObject*>::iterator it;

		//tworzymy now� map� kolizji, kt�ra pokazuje r�wnie� kafelki zaj�te przez b��dy
		for(i=0; i<SCENE_WIDTH * SCENE_HEIGHT; i++) new_collision_map[i] = collision_map[i];
		for(it=enemies.begin(); it!=enemies.end(); it++)
		{
			if((*it) != this)
			{
				tiles = (*it)->GetTiles();
				for(i=0; i<tiles.size(); i++)
				{
					new_collision_map[ tiles[i] ] = 1;
				}
			}
		}
		tiles = p1->GetTiles();
		for(i=0; i<tiles.size(); i++)
		{
			new_collision_map[ tiles[i] ] = 2;
		}

		// okre�la, kt�ry kierunek jest najkr�tsz� drog� do gracza, je�li jest on osi�galny
		std::vector<int> occupied_tiles = GetTiles();
		float dist, min_dist;
		int best_state;
		bool illegal_move = false;
		std::vector<int> choices(0);

		min_dist = sqrt(pow(GetX() - p1->GetX(),2.0f) + pow((GetY()+TILE_SIZE) - p1->GetY(),2.0f));
		best_state = STATE_WALKUP;
		dist = sqrt(pow(GetX() - p1->GetX(),2.0f) + pow((GetY()-TILE_SIZE) - p1->GetY(),2.0f));
		if(dist < min_dist) {min_dist = dist; best_state = STATE_WALKDOWN;}
		dist = sqrt(pow((GetX()+TILE_SIZE) - p1->GetX(),2.0f) + pow(GetY() - p1->GetY(),2.0f));
		if(dist < min_dist) {min_dist = dist; best_state = STATE_WALKRIGHT;}
		dist = sqrt(pow((GetX()-TILE_SIZE) - p1->GetX(),2.0f) + pow(GetY() - p1->GetY(),2.0f));
		if(dist < min_dist) {min_dist = dist; best_state = STATE_WALKLEFT;}

		for(i=0; i<occupied_tiles.size() && !illegal_move; i++) if(new_collision_map[occupied_tiles[i] + SCENE_WIDTH]) illegal_move=true;
		if(!illegal_move) choices.push_back(STATE_WALKUP);
		else illegal_move = false;
		for(i=0; i<occupied_tiles.size() && !illegal_move; i++) if(new_collision_map[occupied_tiles[i] - SCENE_WIDTH]) illegal_move=true;
		if(!illegal_move) choices.push_back(STATE_WALKDOWN);
		else illegal_move = false;
		for(i=0; i<occupied_tiles.size() && !illegal_move; i++) if(new_collision_map[occupied_tiles[i] + 1]) illegal_move=true;
		if(!illegal_move) choices.push_back(STATE_WALKRIGHT);
		else illegal_move = false;
		for(i=0; i<occupied_tiles.size() && !illegal_move; i++) if(new_collision_map[occupied_tiles[i] - 1]) illegal_move=true;
		if(!illegal_move) choices.push_back(STATE_WALKLEFT);
		else illegal_move = false;

		if(choices.size()) 
		{
			SetState(choices[rand()%choices.size()]); //przypisuje losowy adres z mo�liwych
			for(i=0; i<choices.size(); i++) if(choices[i] == best_state) SetState(best_state); // je�li jeden z tych mo�liwych kierunk�w jest najlepszy, przypisz ten kierunek
		    Move(); //Zaczynam porusza� si� w wyznaczonym kierunku, je�li w og�le, aby odr�ni� si� od przypadku, gdy dotr� do nowego kafelka
		}
	}
	return false;
}