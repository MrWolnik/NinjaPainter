#pragma once

#include "Player1.h"
#include "Player2.h"
#include "Monster1.h"
#include "Monster2.h"
#include "Bullet.h"
#include "Token.h"
#include "Overlay.h"
#include "Data.h"
#include "Sound.h"
#include <fstream>

//wymiary okna pocz�tkowego
#define GAME_WIDTH	((SCENE_WIDTH-2)*TILE_SIZE + SCENE_Xo*2) //warto�ci sk�adaj�ce si� z #define zawsze uj�te w nawias
#define GAME_HEIGHT int((SCENE_WIDTH-2)*TILE_SIZE*1.5f + SCENE_Yo*2)

#define STATE_GAMEOVER    0
#define STATE_RUN         1
#define STATE_PAUSE       2
#define STATE_MAINMENU    3
#define STATE_LEVELCHANGE 4
#define STATE_ENDGAME	  5
#define STATE_MENU        6

#define TOTAL_LEVELS	  2
#define DYNAMIC_FILENAME  "dynamic_layer"

//kontrolki
#define P1_UP		GLUT_KEY_UP
#define P1_DOWN		GLUT_KEY_DOWN
#define P1_LEFT		GLUT_KEY_LEFT
#define P1_RIGHT	GLUT_KEY_RIGHT
#define P1_ATTACK	'z'
#define P1_SKILL	'x'

#define BONUS_DURATION 7000

#define ARTEFACT_AMOUNT 5

class Game
{
public:
	Game(void);
	virtual ~Game(void);

	bool Init(int level);
	bool Loop();
	void Finalize();
	void setTempData(int tab[], int points);
	void setTempArtefacts(int tab[]);

	//Wej�cie
	void ReadKeyboard(unsigned char key, int x, int y, bool press);
	void ReadSpecialKeyboard(unsigned char key, int x, int y, bool press);
	void ReadMouse(int button, int state, int x, int y);
	//Proces
	bool Process(int t1);
	void Reshape(int w,int h);
	//Wyj�cie
	void Render();

	bool firstInitialization;
	bool skipMusicInitialization;
	int tempArtefacts[5];

private:
	unsigned char keys[256];
	Rect visible_area;
	int state,level;
	bool p1_attacks,p2_attacks;
	Scene Scene;
	Player* player1;
	Overlay Overlay1;
	Overlay Overlay2;
	Overlay Overlay3;
	Overlay Overlay4;
	std::list<GObject*> enemies;
	std::list<Bullet*> projectiles;
	std::list<Token*> tokens;
	Data Data;
	Sound Sound;
	float time;
	bool ia, epilepsia_mode;
	int bonusStartTime;
	int* artefactCounter;
	bool changePlayer = false;
	bool hasPlayerWon = false;
	bool hasPlayerLose = false;

	void UpdateCamera(int h1);
	bool LoadDynamicLayer(int lvl);
};
