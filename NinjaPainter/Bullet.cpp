#include "Bullet.h"

Bullet::Bullet()
{
	seq = 0;
	delay = 0;
	frame_delay = 3;
	step_length = STEP_LENGTH*4;
	displace_up=false; displace_down=false;
}
Bullet::~Bullet(){}

void Bullet::SetPosition(int posx,int posy)
{
	x = posx;
	y = posy;
}
int Bullet::GetY()
{
	return y;
}
int Bullet::GetX()
{
	return x;
}
void Bullet::SetHitHeight(int hh)
{
	hit_h = hh;
}
int Bullet::GetHitHeight()
{
	return hit_h;
}
void Bullet::SetHitWidth(int hw)
{
	hit_w = hw;
}
int Bullet::GetHitWidth()
{
	return hit_w;
}
void Bullet::SetFrameTexTiles(int ftt)
{
	frame_texture_tiles = ftt;
}
int Bullet::GetFrameTexTiles()
{
	return frame_texture_tiles;
}
Rect Bullet::GetFrameBox()
{
	Rect framebox;
	framebox.top = y + hit_h + (frame_texture_tiles*TILE_SIZE - hit_h)/2;
	framebox.bottom = y - (frame_texture_tiles*TILE_SIZE - hit_h)/2;
	framebox.left =  x - (frame_texture_tiles*TILE_SIZE - hit_w)/2;
	framebox.right = x  + hit_w + (frame_texture_tiles*TILE_SIZE - hit_h)/2;
	return framebox;
}
Rect Bullet::GetHitBox()
{
	Rect hitbox;
	hitbox.top = y + hit_h;
	hitbox.bottom = y;
	hitbox.left = x;
	hitbox.right = x + hit_w;
	return hitbox;
}
bool Bullet::Intersection(Rect box1, Rect box2)
{
	if(box1.top > box2.bottom && box2.top > box1.bottom)
	{
		if(box1.left < box2.right && box2.left < box1.right) return true;
	}
	return false;
}

bool Bullet::CollidesObjUD(bool *proj_collision_map,bool up)
{	
	int tile_x,tile_y;
	int width_tiles;

	tile_x = x / TILE_SIZE;
	if(up && ((y + hit_h) / TILE_SIZE) < SCENE_HEIGHT) tile_y = (y + hit_h) / TILE_SIZE;
	else tile_y = y / TILE_SIZE;

	width_tiles = 1 + ((hit_w - 1) / TILE_SIZE); // zaokr�glenie
	if( (x%TILE_SIZE + hit_w) > TILE_SIZE ) width_tiles++;

	for(int j=0;j<width_tiles;j++)
	{
		if(proj_collision_map[ (tile_x+j) + tile_y*SCENE_WIDTH ]) return true;
	}
	return false;
}

bool Bullet::CollidesObjLR(bool *proj_collision_map,bool right)
{
	int tile_x,tile_y;
	int height_tiles;

	if(right && ((x + hit_w) / TILE_SIZE) < SCENE_WIDTH) tile_x = (x + hit_w) / TILE_SIZE;
	else tile_x = x / TILE_SIZE;
	tile_y = y / TILE_SIZE;

	height_tiles = 1 + ((hit_h - 1) / TILE_SIZE); // zaokr�glenie
	if( (y%TILE_SIZE + hit_h) > TILE_SIZE ) height_tiles++;

	for(int j=0;j<height_tiles;j++)
	{
		if(proj_collision_map[ tile_x + (tile_y+j)*SCENE_WIDTH ]) return true;
	}
	return false;
}

void Bullet::LevitateUp(bool *proj_collision_map)
{
	if( (y % TILE_SIZE) == 0)
	{
		int yaux = y;
		y += step_length;
		if(CollidesObjUD(proj_collision_map,true))
		{
			y = yaux;
			state = STATE_ENDINGUP; // je�li uderzy w obiekt, to wybuchnie
			seq = 0;
			delay = 0;
		}
	}
	else y += step_length;
}
void Bullet::LevitateDown(bool *proj_collision_map)
{
	if( (y % TILE_SIZE) == 0)
	{
		int yaux = y;
		y -= step_length;
		if(CollidesObjUD(proj_collision_map,false))
		{
			y = yaux;
			state = STATE_ENDINGDOWN;
			seq = 0;
			delay = 0;
		}
	}
	else y -= step_length;
}
void Bullet::LevitateLeft(bool *proj_collision_map)
{
	if( (x % TILE_SIZE) == 0)
	{
		int xaux = x;
		x -= step_length;
		if(CollidesObjLR(proj_collision_map,false))
		{
			x = xaux;
			state = STATE_ENDINGLEFT; 
			seq = 0;
			delay = 0;
		}
	}
	else x -= step_length;
}
void Bullet::LevitateRight(bool *proj_collision_map)
{
	if( (x % TILE_SIZE) == 0)
	{
		int xaux = x;
		x += step_length;
		if(CollidesObjLR(proj_collision_map,true))
		{
			x = xaux;
			state = STATE_ENDINGRIGHT; 
			seq = 0;
			delay = 0;
		}
	}
	else x += step_length;
}

bool Bullet::ComputeEnemyCollisions(std::list<GObject*> &enemies)
{
	bool collision = false;
	std::list<GObject*>::iterator it;
	for(it=enemies.begin(); it!=enemies.end(); it++)
	{
		if((*it)->GetState() != STATE_DYING && Intersection(GetHitBox(),(*it)->GetHitBox()) ) 
		{
			Player1* player1 = dynamic_cast<Player1*>(*it);
			if(player1 != NULL && player1->IsDoingSkill())
			{
				if(state == STATE_LEVITATEUP)		  player1->TestShieldProtection(STATE_ATTACKUP,GetDamage(),GetX(),GetY());
				else if(state == STATE_LEVITATEDOWN)  player1->TestShieldProtection(STATE_ATTACKDOWN,GetDamage(),GetX(),GetY());
				else if(state == STATE_LEVITATELEFT)  player1->TestShieldProtection(STATE_ATTACKLEFT,GetDamage(),GetX(),GetY());
				else if(state == STATE_LEVITATERIGHT) player1->TestShieldProtection(STATE_ATTACKRIGHT,GetDamage(),GetX(),GetY());
			}
			else
			{
				int new_health = (*it)->GetHealth() - GetDamage();
				if(new_health <= 0) (*it)->SetState(STATE_DYING);
				else if(dynamic_cast<Player*>(*it) == NULL || !dynamic_cast<Player*>(*it)->IsSuperAttacking())
				{
					if(state == STATE_LEVITATEUP)		  (*it)->SetState(STATE_DAMAGEDOWN);
					else if(state == STATE_LEVITATEDOWN)  (*it)->SetState(STATE_DAMAGEUP);
					else if(state == STATE_LEVITATELEFT)  (*it)->SetState(STATE_DAMAGERIGHT);
					else if(state == STATE_LEVITATERIGHT) (*it)->SetState(STATE_DAMAGELEFT);
					(*it)->SetFrame(0);
					(*it)->SetDelay(0);
				}
				if(new_health < 0) new_health = 0;
				(*it)->SetHealth(new_health);
			}
			collision = true;
		}
	}
	return collision;
}

bool Bullet::Logic(bool *proj_collision_map,std::list<GObject*> &enemies)
{
	if(!IsEnding())
	{
		if(ComputeEnemyCollisions(enemies))
		{
			// je�li zderzy si� z wrogiem, rozpocznie si� animacja ko�cz�ca
			if(state == STATE_LEVITATEUP)		  state = STATE_ENDINGUP;
			else if(state == STATE_LEVITATEDOWN)  state = STATE_ENDINGDOWN;
			else if(state == STATE_LEVITATERIGHT) state = STATE_ENDINGRIGHT;
			else if(state == STATE_LEVITATELEFT)  state = STATE_ENDINGLEFT;
			seq = 0;
			delay = 0;
			return true;
		}
		else
		{
			if(state == STATE_LEVITATEUP || state == STATE_LEVITATEDOWN)
			{
				if(state == STATE_LEVITATEUP) LevitateUp(proj_collision_map);
				else LevitateDown(proj_collision_map);
			}
			else
			{
				if(state == STATE_LEVITATELEFT) LevitateLeft(proj_collision_map);
				else LevitateRight(proj_collision_map);
				
				if(displace_up) LevitateUp(proj_collision_map);
				else if(displace_down) LevitateDown(proj_collision_map);
			}
			if(IsEnding()) return true;
		}
	}
	return false;
}

void Bullet::DrawRect(int tex_id,float xo,float yo,float xf,float yf)
{
	int screen_x,screen_y;

	screen_x = x + SCENE_Xo;
	screen_y = y + SCENE_Yo;

	glEnable(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D,tex_id);
	glBegin(GL_QUADS);
		glTexCoord2f(xo,yo);	glVertex3i(screen_x - (TILE_SIZE*frame_texture_tiles - hit_w)/2,screen_y - (TILE_SIZE*frame_texture_tiles - hit_h)/2,screen_y); //z nieco wy�ej, aby da� poczucie przebywania ponad poziomem gruntu
		glTexCoord2f(xf,yo);	glVertex3i(screen_x + (TILE_SIZE*frame_texture_tiles + hit_w)/2,screen_y - (TILE_SIZE*frame_texture_tiles - hit_h)/2,screen_y);
		glTexCoord2f(xf,yf);	glVertex3i(screen_x + (TILE_SIZE*frame_texture_tiles + hit_w)/2,screen_y + (TILE_SIZE*frame_texture_tiles + hit_h)/2,screen_y);
		glTexCoord2f(xo,yf);	glVertex3i(screen_x - (TILE_SIZE*frame_texture_tiles - hit_w)/2,screen_y + (TILE_SIZE*frame_texture_tiles + hit_h)/2,screen_y);
	glEnd();

	glDisable(GL_TEXTURE_2D);
}

int Bullet::GetState()
{
	return state;
}
void Bullet::SetState(int s)
{
	state = s;
}

void Bullet::NextFrame(int max)
{
	delay++;
	if(delay == frame_delay)
	{
		seq++;
		seq%=max;
		delay = 0;
	}
}
int Bullet::GetFrame()
{
	return seq;
}
int Bullet::GetDelay()
{
	return delay;
}

void Bullet::SetDamage(int d)
{
	damage = d;
}
int Bullet::GetDamage()
{
	return damage;
}

int Bullet::GetStepLength()
{
	return step_length;
}

void Bullet::SetDisplaceUp(bool b)
{
	displace_up = b;
}
void Bullet::SetDisplaceDown(bool b)
{
	displace_down = b;
}

bool Bullet::IsEnding()
{
	if(state == STATE_ENDINGUP    || state == STATE_ENDINGDOWN || 
	   state == STATE_ENDINGRIGHT || state == STATE_ENDINGLEFT  ) return true;
	else return false;
}