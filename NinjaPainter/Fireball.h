#pragma once

#include "Bullet.h"

class Fireball: public Bullet
{
public:
	Fireball();
	~Fireball();

	void Draw(int tex_id,int tex_w,int tex_h,bool run);
};