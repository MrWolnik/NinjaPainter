#pragma once

#include "Scene.h"

#define STEP_LENGTH	  (TILE_SIZE/8)

#define STATE_LOOKUP		1
#define STATE_LOOKRIGHT		2
#define STATE_LOOKDOWN		3
#define STATE_LOOKLEFT		4

#define STATE_WALKUP		6
#define STATE_WALKRIGHT		7
#define STATE_WALKDOWN		8
#define STATE_WALKLEFT		9

#define STATE_DAMAGEUP		10
#define STATE_DAMAGERIGHT	11
#define STATE_DAMAGEDOWN	12
#define STATE_DAMAGELEFT	13

#define STATE_ATTACKUP		14
#define STATE_ATTACKRIGHT	15
#define STATE_ATTACKDOWN	16
#define STATE_ATTACKLEFT	17

#define STATE_DYING			0

class GObject
{
public:
	GObject(void);
	virtual ~GObject(void);

	void SetPosition(int x,int y);
	void SetX(int x);
	int  GetX();
	void SetY(int y);
	int  GetY();
	void SetBaseTilesHeight(int bth);
	int  GetBaseHeight();
	void SetBaseTilesWidth(int btw);
	int  GetBaseWidth();
	void SetHitHeight(int hh);
	int GetHitHeight();
	void SetHitWidth(int hw);
	int GetHitWidth();


	std::vector<int> GetTiles(); //zwraca pozycje kafelk�w przeci�tych przez podstaw� obiektu GObject  
	Rect GetHitBox();
	bool Intersection(Rect box1, Rect box2);
	bool Intersection(Rect box1,int px,int py);
	void SetTile(int tx,int ty);

	virtual void Draw(int tex_id,int tex_w,int tex_h,bool run) {}; //w przypadku wywo�ania funkcji wirtualnej zostanie wykonana funkcja o tej samej nazwie z odpowiedniej podklasy

	void Stop();

	void StartAttack();

	void SetState(int s);
	int  GetState();

	void SetFrame(int frame);
	int  GetFrame();

	void SetDelay(int d);
	int  GetDelay();

	bool IsLooking();
	bool IsWalking();
	bool IsAttacking();
	bool IsDamaged();
	
	void SetMaxHealth(int max_h);
	int  GetMaxHealth();
	void SetHealth(int health);
	int  GetHealth();
	void SetDamage(int damage);
	int  GetDamage();
	
private:
	int x,y, //lewy dolny piksel hitboxa b��du
		hit_w,hit_h, //wymiary w pikselach trafionej cz�ci klatki sprite'a, nie musz� by� podzielne przez TILE_SIZE, ale w powi�zanych sprite'ach hitbox musi by� w centrum/�rodku ka�dej klatki 
		base_tiles_w, base_tiles_h, //wymiary w kafelkach podstawy kolizji
		state,
		seq,delay,//seq to klatka sekwencji animacji, kt�ra ma by� teraz malowana, delay to czas oczekiwania od jednej z tych klatek do nast�pnej
		max_health,health,damage;
};
