#include "Data.h"

Data::Data(void) {}
Data::~Data(void){}


/** \brief funkcja zwraca warto�� id z tablicy obrazow*/
int Data::GetID(int img)
{
	return textures[img].GetID();
}


/** \brief funkcja zwraca rozmiar tekstury*/
void Data::GetSize(int img, int *w, int *h)
{
	textures[img].GetSize(w,h);
}

bool Data::LoadImage(int img, char *filename, int type)
{
	int res;

	res = textures[img].Load(filename,type);
	if(!res) return false;
	return true;
}



bool Data::Load()
{
	std::map<int, const char*> texturesMap = {
		{IMG_TILESET, "Textures/tileset.png"},
		{IMG_PLAYER1, "Textures/player1.png"},
		{IMG_SUPERP1, "Textures/superp1.png"},
		{IMG_PLAYER2, "Textures/player2.png"},
		{IMG_OVERLAY1, "Textures/overlay1.png"},
		{IMG_OVERLAY2, "Textures/overlay2.png"},
		{IMG_MONSTER1, "Textures/monster1.png"},
		{IMG_MONSTER2, "Textures/monster2.png"},
		{IMG_FIREBALL, "Textures/fireball.png"},
		{IMG_ARROW,	"Textures/arrow.png"},
		{IMG_HEART,	"Textures/heart.png"},
		{IMG_ARTEFACT1, "Textures/artefact1.png"},
		{IMG_ARTEFACT2, "Textures/artefact2.png"},
		{IMG_ARTEFACT3, "Textures/artefact3.png"},
		{IMG_ARTEFACT4, "Textures/artefact4.png"},
		{IMG_ARTEFACT5, "Textures/artefact5.png"},
		{IMG_GAMEOVER, "Textures/game_over.png"},
		{IMG_WIN, "Textures/win.png"}
	};

	int res;

	for (auto const& item : texturesMap){
		res = LoadImage(item.first, (char*)item.second, GL_RGBA);
		if (!res) { return false; }
	}
		
	return true;
}