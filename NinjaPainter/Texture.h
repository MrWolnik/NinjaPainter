#pragma once

#pragma comment(lib,"Dependencies/corona")

#include "Utils.h"
#include "Dependencies/corona.h"

class Texture
{
public:
	Texture(void);
	~Texture(void);

	bool Load(char *filename,int type = GL_RGBA,int wraps = GL_REPEAT,int wrapt = GL_REPEAT,
			  int magf = GL_NEAREST,int minf = GL_NEAREST,bool mipmap = false);
	int  GetID();
	void GetSize(int *w,int *h);

private:
	GLuint id;
	int width,height;
};
