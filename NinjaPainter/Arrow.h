#pragma once

#include "Bullet.h"

class Arrow: public Bullet
{
public:
	Arrow();
	~Arrow();

	void Draw(int tex_id,int tex_w,int tex_h,bool run);
};