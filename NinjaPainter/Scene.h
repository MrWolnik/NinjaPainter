#pragma once

#include "Utils.h"

#define SCENE_Xo		  0   //czarna granica w px
#define SCENE_Yo		  0
#define SCENE_WIDTH		  20  //kafelki
#define SCENE_HEIGHT	  160

#define LEVEL_FILENAME	  "static_layer"
#define FILENAME_EXT	  ".txt"

#define TILE_FRAMES		  4   //numer klatek, jak� b�d� mia�y animowane kafelki 
#define TILE_DELAY		  3   // ile czasu zajmie przej�cie animowanego kafelka z jednej klatki do drugiej

#define TILE_SIZE		  24   //rozmiar w px boku kafelka na ekranie
#define TEXTURE_TILE_SIZE 16   //rozmiar w px boku kafelka w teksturze (zwykle 16 lub 32)

class Rect
{
public:
	int left,top,
		right,bottom;
};

class Tile
{
public:
	void  SetX(int px){x = px;}
	void  SetY(int py){y = py;}
	void  SetTexCoordX(float tex_x){tex_coordx = tex_x;}
	void  SetTexCoordY(float tex_y){tex_coordy = tex_y;}
	int   GetX(){return x;}
	int   GetY(){return y;}
	float GetTexCoordX(){return tex_coordx;}
	float GetTexCoordY(){return tex_coordy;}

private:
	int x,y; //pozycja ekranu
	float tex_coordx,tex_coordy; //wsp�rz�dne tekstury w tilesecie
};

class Scene
{
public:
	Scene(void);
	virtual ~Scene(void);

	bool LoadLevel(int level,int tex_w,int tex_h);
	void ComputeCollisionMaps();
	void Draw(int tex_id,int tex_w,int tex_h,bool run);
	void DrawAnimatedTiles(int tex_id,int tex_w,int tex_h);
	bool *GetCollisionMap();
	bool *GetProjCollisionMap();

private:

	//zestaw kafelek o r�nych zachowaniach
	int terrain_flat[SCENE_WIDTH * SCENE_HEIGHT];
	int terrain_solid[SCENE_WIDTH * SCENE_HEIGHT];
	int solid_alpha[SCENE_WIDTH * SCENE_HEIGHT];
	std::vector<Tile> animated_tiles;
	std::vector<Tile> animated_tiles_up;
	int bridges[SCENE_WIDTH * SCENE_HEIGHT];
	int always_down[SCENE_WIDTH * SCENE_HEIGHT];
	int z_tileup[SCENE_WIDTH * SCENE_HEIGHT];
	int z_tiledown[SCENE_WIDTH * SCENE_HEIGHT];
	int always_up[SCENE_WIDTH * SCENE_HEIGHT];
	int always_up_alpha[SCENE_WIDTH * SCENE_HEIGHT];

	//wy�wietlanie list
	int id_TerrainFlat;
	int id_TerrainSolid;
	int id_SolidAlpha;
	int id_Bridges;
	int id_AlwaysDown;
	int id_ZtileUp;
	int id_ZtileDown;
	int id_AlwaysUp;
	int id_AlwaysUpAlpha;

	bool collision_map[SCENE_WIDTH * SCENE_HEIGHT];
	bool proj_collision_map[SCENE_WIDTH * SCENE_HEIGHT];
	int  seq,delay,frame_delay;
};
