#include "GObject.h"

GObject::GObject(void)
{
	seq = 0;
	delay = 0;
	state = STATE_LOOKDOWN;
}
GObject::~GObject(void){}

void GObject::SetPosition(int posx,int posy)
{
	x = posx;
	y = posy;
}
void GObject::SetX(int posx)
{
	x = posx;
}
int GObject::GetX()
{
	return x;
}
void GObject::SetY(int posy)
{
	y = posy;
}
int GObject::GetY()
{
	return y;
}
void GObject::SetBaseTilesHeight(int bth)
{
	base_tiles_h = bth;
}
int GObject::GetBaseHeight()
{
	return base_tiles_h*TILE_SIZE;
}
void GObject::SetBaseTilesWidth(int btw)
{
	base_tiles_w = btw;
}
int GObject::GetBaseWidth()
{
	return base_tiles_w*TILE_SIZE;
}
void GObject::SetHitHeight(int hh)
{
	hit_h = hh;
}
int GObject::GetHitHeight()
{
	return hit_h;
}
void GObject::SetHitWidth(int hw)
{
	hit_w = hw;
}
int GObject::GetHitWidth()
{
	return hit_w;
}
std::vector<int> GObject::GetTiles() //w zale�no�ci od wymiar�w podstawy
{
	int i,j;
	std::vector<int> tiles;
	int tile_x,tile_y;  //s� to wsp�rz�dne w kafelkach, gdzie znajduje si� b��d
	int occupied_tiles_w,occupied_tiles_h; //S� numerami kafli ziemi zajmowanych przez baz� b��d�w.
	
	tile_x = x/TILE_SIZE;
	tile_y = y/TILE_SIZE;
	occupied_tiles_w = base_tiles_w;
	occupied_tiles_h = base_tiles_h;
	if((x%TILE_SIZE) != 0) occupied_tiles_w++;
	if((y%TILE_SIZE) != 0) occupied_tiles_h++;

	for(i=0; i<occupied_tiles_w; i++)
	{
		for(j=0; j<occupied_tiles_h; j++) tiles.push_back( (tile_x+i) + ((tile_y+j)*SCENE_WIDTH) );
	}
	return tiles;
}

Rect GObject::GetHitBox()
{
	Rect hitbox;
	hitbox.top = y + hit_h;
	hitbox.bottom = y;
	hitbox.left = x;
	hitbox.right = x + hit_w;
	return hitbox;
}
bool GObject::Intersection(Rect box1, Rect box2)
{
	if(box1.top > box2.bottom && box2.top > box1.bottom)
	{
		if(box1.left < box2.right && box2.left < box1.right) return true;
	}
	return false;
}
bool GObject::Intersection(Rect box1, int px, int py)
{
	if(px >= box1.left && px < box1.right && py >= box1.bottom && py < box1.top) return true; 
	return false;
}
void GObject::SetTile(int tx,int ty)
{
	x = tx * TILE_SIZE;
	y = ty * TILE_SIZE;
}

void GObject::Stop()
{
	switch(state)
	{
		case STATE_WALKUP:		state = STATE_LOOKUP;		break;
		case STATE_WALKRIGHT:	state = STATE_LOOKRIGHT;	break;
		case STATE_WALKDOWN:	state = STATE_LOOKDOWN;		break;
		case STATE_WALKLEFT:	state = STATE_LOOKLEFT;		break;

		case STATE_ATTACKUP:	state = STATE_LOOKUP;		break;
		case STATE_ATTACKRIGHT:	state = STATE_LOOKRIGHT;	break;
		case STATE_ATTACKDOWN:	state = STATE_LOOKDOWN;		break;
		case STATE_ATTACKLEFT:	state = STATE_LOOKLEFT;		break;

		case STATE_DAMAGEUP:	state = STATE_LOOKUP;		break;
		case STATE_DAMAGERIGHT:	state = STATE_LOOKRIGHT;	break;
		case STATE_DAMAGEDOWN:	state = STATE_LOOKDOWN;		break;
		case STATE_DAMAGELEFT:	state = STATE_LOOKLEFT;		break;
	}
}

void GObject::StartAttack()
{
	if(state == STATE_LOOKUP	|| state == STATE_WALKUP)	 state = STATE_ATTACKUP;
	if(state == STATE_LOOKDOWN  || state == STATE_WALKDOWN)  state = STATE_ATTACKDOWN;
	if(state == STATE_LOOKLEFT  || state == STATE_WALKLEFT)  state = STATE_ATTACKLEFT;
	if(state == STATE_LOOKRIGHT || state == STATE_WALKRIGHT) state = STATE_ATTACKRIGHT;

	seq = 0;
	delay = 0;
}

void GObject::SetFrame(int frame)
{
	seq = frame;
}
int GObject::GetFrame()
{
	return seq;
}
int GObject::GetState()
{
	return state;
}
void GObject::SetState(int s)
{
	state = s;
}
void GObject::SetDelay(int d)
{
	delay = d;
}
int GObject::GetDelay()
{
	return delay;
}

void GObject::SetMaxHealth(int max_h)
{
	max_health = max_h;
}
int GObject::GetMaxHealth()
{
	return max_health;
}
void GObject::SetHealth(int h)
{
	health = h;
}
int GObject::GetHealth()
{
	return health;
}
void GObject::SetDamage(int d)
{
	damage = d;
}
int GObject::GetDamage()
{
	return damage;
}

bool GObject::IsLooking()
{
	if( state == STATE_LOOKUP   || state == STATE_LOOKDOWN || 
	    state == STATE_LOOKLEFT || state == STATE_LOOKRIGHT ) return true;
	return false;
}
bool GObject::IsWalking()
{
	if( state == STATE_WALKUP   || state == STATE_WALKDOWN || 
	    state == STATE_WALKLEFT || state == STATE_WALKRIGHT ) return true;
	return false;
}
bool GObject::IsAttacking()
{
	if( state == STATE_ATTACKUP   || state == STATE_ATTACKDOWN  || 
	    state == STATE_ATTACKLEFT || state == STATE_ATTACKRIGHT  ) return true;
	return false;
}
bool GObject::IsDamaged()
{
	if( state == STATE_DAMAGEUP   || state == STATE_DAMAGEDOWN || 
	    state == STATE_DAMAGELEFT || state == STATE_DAMAGERIGHT ) return true;
	return false;
}