#include "Monster2.h"

Monster2::Monster2()
{
	SetBaseTilesHeight(1);
	SetBaseTilesWidth(1);
	SetHitHeight(TILE_SIZE+TILE_SIZE/2);
	SetHitWidth(TILE_SIZE);
	SetFrameTexTiles(4);
	SetMaxHealth(30);
	SetHealth(30);
}
Monster2::~Monster2(){}

/* \brief funkcja Attack - atakowanie gracza kamienna kula przez przeciwnika2 */
bool Monster2::Attack(std::list<GObject*> &players,std::list<Bullet*> &projectiles,bool *proj_collision_map)
{
	
	if(GetFrame() == 6 && GetDelay() == 0)
	{
		int x,y;
		projectiles.push_back(new Arrow);

		if(GetState() == STATE_ATTACKUP)
		{
			x = GetX() + (GetBaseWidth() - projectiles.back()->GetHitWidth())/2;
			y = GetY() + GetBaseHeight();
			projectiles.back()->SetState(STATE_LEVITATEUP);
			projectiles.back()->SetPosition(x,y);
			//ocenia, czy pocisk pojawia si� na przeciwniku lub obiekcie
			if( projectiles.back()->ComputeEnemyCollisions(players) || projectiles.back()->CollidesObjUD(proj_collision_map,false) || projectiles.back()->CollidesObjUD(proj_collision_map,true))
				projectiles.back()->SetState(STATE_ENDINGUP);
		}
		else if(GetState() == STATE_ATTACKDOWN)
		{
			x = GetX() + (GetBaseWidth() - projectiles.back()->GetHitWidth())/2;
			y = GetY();
			projectiles.back()->SetState(STATE_LEVITATEDOWN);
			projectiles.back()->SetPosition(x,y);
			//ocenia, czy pocisk pojawia si� na przeciwniku lub obiekcie
			if( projectiles.back()->ComputeEnemyCollisions(players) || projectiles.back()->CollidesObjUD(proj_collision_map,false) )
				projectiles.back()->SetState(STATE_ENDINGDOWN);
		}
		else if(GetState() == STATE_ATTACKLEFT)
		{
			x = GetX();
			y = GetY() + (GetBaseHeight() - projectiles.back()->GetHitHeight())/2;
			projectiles.back()->SetState(STATE_LEVITATELEFT);
			projectiles.back()->SetPosition(x,y);
			//ocenia, czy pocisk pojawia si� na przeciwniku lub obiekcie
			if( projectiles.back()->ComputeEnemyCollisions(players) || projectiles.back()->CollidesObjLR(proj_collision_map,false) )
				projectiles.back()->SetState(STATE_ENDINGLEFT);
		}
		else if(GetState() == STATE_ATTACKRIGHT)
		{
			x = GetX() + GetBaseWidth();
			y = GetY() + (GetBaseHeight() - projectiles.back()->GetHitHeight())/2;
			projectiles.back()->SetState(STATE_LEVITATERIGHT);
			projectiles.back()->SetPosition(x,y);
			//ocenia, czy pocisk pojawia si� na przeciwniku lub obiekcie
			if( projectiles.back()->ComputeEnemyCollisions(players) || projectiles.back()->CollidesObjLR(proj_collision_map,false) || projectiles.back()->CollidesObjLR(proj_collision_map,true))
				projectiles.back()->SetState(STATE_ENDINGRIGHT);
		}
		return true;
	}
	return false;
}

/* \brief funkcja atakowania - sprawdzenie czy gracz jest osiagalny i mo�liwy jest atak */
bool Monster2::IA(bool *collision_map,bool *proj_collision_map,std::list<GObject*> &enemies,GObject* p1)
{
	int i;
	bool collision=false;
	Arrow arrow;
	int arrow_x,arrow_y;
	Rect hitbox_p1 = p1->GetHitBox();

	arrow_x = GetX() + (GetBaseWidth() - arrow.GetHitWidth())/2;
	arrow_y = GetY() + (GetBaseHeight() - arrow.GetHitHeight())/2;

	// je�li jaki� gracz jest osi�galny, przypisuj� mu odpowiedni status ataku.
	if(hitbox_p1.right > arrow_x && hitbox_p1.left < arrow_x + arrow.GetHitWidth())
	{
		if(hitbox_p1.bottom > GetY())
		{
			for(i=((GetY()+GetBaseHeight())/TILE_SIZE); i<=(hitbox_p1.bottom/TILE_SIZE) && !collision; i++)
			{
			 collision = proj_collision_map[ (GetX()/TILE_SIZE) + i*SCENE_WIDTH ];
			}
			if(!collision)
			{
				SetState(STATE_ATTACKUP);
				SetFrame(0);
				SetDelay(0);
				return true;
			}
		}
		else
		{
			for(i=(hitbox_p1.bottom/TILE_SIZE); i<=((GetY()-GetBaseHeight())/TILE_SIZE) && !collision; i++)
			{
			 collision = proj_collision_map[ (GetX()/TILE_SIZE) + i*SCENE_WIDTH ];
			}
			if(!collision)
			{
				SetState(STATE_ATTACKDOWN);
				SetFrame(0);
				SetDelay(0);
				return true;
			}
		}
	}
	else if(hitbox_p1.top > arrow_y && hitbox_p1.bottom < arrow_y + arrow.GetHitHeight())
	{
		if(hitbox_p1.left > GetX())
		{
			for(i=((GetX()+GetBaseWidth())/TILE_SIZE); i<=(hitbox_p1.left/TILE_SIZE) && !collision; i++)
			{
			 collision = proj_collision_map[ i + (GetY()/TILE_SIZE)*SCENE_WIDTH ];
			}
			if(!collision)
			{
				SetState(STATE_ATTACKRIGHT);
				SetFrame(0);
				SetDelay(0);
				return true;
			}
		}
		else
		{
			for(i=(hitbox_p1.left/TILE_SIZE); i<=((GetX()-GetBaseWidth())/TILE_SIZE) && !collision; i++)
			{
			 collision = proj_collision_map[ (GetX()/TILE_SIZE) + i*SCENE_WIDTH ];
			}
			if(!collision)
			{
				SetState(STATE_ATTACKLEFT);
				SetFrame(0);
				SetDelay(0);
				return true;
			}
		}
	}

	// Je�li nie mo�e zaatakowa�, obliczam �cie�k� i przypisuj� odpowiedni stan ruchu.
	//Je�li zosta� zaatakowany podczas ruchu, m�g� zosta� przesuni�ty poza kraw�dzie p�ytki, nale�y go ponownie umie�ci�
	if(GetX()%TILE_SIZE != 0)
	{
		if(GetX()%TILE_SIZE < TILE_SIZE/2) SetState(STATE_WALKLEFT);
		else SetState(STATE_WALKRIGHT);
	}
	else if(GetY()%TILE_SIZE != 0)
	{
		if(GetY()%TILE_SIZE < TILE_SIZE/2) SetState(STATE_WALKDOWN);
		else SetState(STATE_WALKUP);
	}
	else
	{
		int new_collision_map[SCENE_WIDTH * SCENE_HEIGHT]; //0 dla pustych, 1 dla przeszk�d, 2 dla gracza
		std::vector<int> tiles;
		std::list<GObject*>::iterator it;

		//tworzymy now� map� kolizji, kt�ra pokazuje r�wnie� kafelki zaj�te przez b��dy
		for(i=0; i<SCENE_WIDTH * SCENE_HEIGHT; i++) new_collision_map[i] = collision_map[i];
		for(it=enemies.begin(); it!=enemies.end(); it++)
		{
			if((*it) != this)
			{
				tiles = (*it)->GetTiles();
				for(i=0; i<tiles.size(); i++)
				{
					new_collision_map[ tiles[i] ] = 1;
				}
			}
		}
		tiles = p1->GetTiles();
		for(i=0; i<tiles.size(); i++)
		{
			new_collision_map[ tiles[i] ] = 2;
		}

		// okre�la, kt�ry kierunek jest najkr�tsz� drog� do gracza, je�li jest on osi�galny
		std::vector<int> occupied_tiles = GetTiles();
		float dist, min_dist;
		int best_state;
		bool illegal_move = false;
		std::vector<int> choices(0);

		min_dist = abs((GetX()+TILE_SIZE) - p1->GetX());
		best_state = STATE_WALKRIGHT;

		for(i=0; i<occupied_tiles.size() && !illegal_move; i++) if(new_collision_map[occupied_tiles[i] + SCENE_WIDTH]) illegal_move=true;
		if(!illegal_move) choices.push_back(STATE_WALKUP);
		else illegal_move = false;
		for(i=0; i<occupied_tiles.size() && !illegal_move; i++) if(new_collision_map[occupied_tiles[i] - SCENE_WIDTH]) illegal_move=true;
		if(!illegal_move) choices.push_back(STATE_WALKDOWN);
		else illegal_move = false;
		for(i=0; i<occupied_tiles.size() && !illegal_move; i++) if(new_collision_map[occupied_tiles[i] + 1]) illegal_move=true;
		if(!illegal_move) choices.push_back(STATE_WALKRIGHT);
		else illegal_move = false;
		for(i=0; i<occupied_tiles.size() && !illegal_move; i++) if(new_collision_map[occupied_tiles[i] - 1]) illegal_move=true;
		if(!illegal_move) choices.push_back(STATE_WALKLEFT);
		else illegal_move = false;

		if (choices.size())
		{
			SetState(choices[rand() % choices.size()]); //przypisuje losowy adres z mo�liwych
			for (i = 0; i < choices.size(); i++) if (choices[i] == best_state) SetState(best_state); // je�li jeden z tych mo�liwych kierunk�w jest najlepszy, przypisz ten kierunek
			Move(); //Zaczynam porusza� si� w wyznaczonym kierunku, je�li w og�le, aby odr�ni� si� od przypadku, w kt�rym dochodz� do nowego kafelka
		}
	}
	return false;
}