#pragma once

#include "Texture.h"
#include <map>
#include <string>

//Rozmiar tablicy obraz�w
#define NUM_TEX		 19

//Image identifiers
#define IMG_TILESET	 0
#define IMG_PLAYER1	 1
#define IMG_SUPERP1  2
#define IMG_PLAYER2	 3
#define IMG_OVERLAY1 5
#define IMG_OVERLAY2 6
#define IMG_MONSTER1 7
#define IMG_MONSTER2 8
#define IMG_FIREBALL 9
#define IMG_ARROW	 10
#define IMG_HEART	 11
#define IMG_ARTEFACT1 12
#define IMG_ARTEFACT2 13
#define IMG_ARTEFACT3 14
#define IMG_ARTEFACT4 15
#define IMG_ARTEFACT5 16
#define IMG_GAMEOVER 17
#define IMG_WIN 18

class Data
{
public:
	Data(void);
	~Data(void);

	int  GetID(int img);
	void GetSize(int img, int *w, int *h);
	bool LoadMenuImage(char* filename, int type);
	bool Load();

private:
	Texture textures[NUM_TEX];
	bool LoadImage(int img,char *filename,int type = GL_RGBA);
};
