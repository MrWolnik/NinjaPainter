#pragma once

#include "Player.h"

class Player1: public Player
{
public:
	Player1();
	~Player1();


	bool Attack(GObject* enemy);

	void SetShieldState(int render_state);

	void TestShieldProtection(int attack_state,int damage,int x,int y);


private:
};