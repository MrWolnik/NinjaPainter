#pragma once

#include "GObject.h"

#define CHARGE_BREAK   30
#define COOLDOWN		6 //minimalna ilo�� ramek oczekiwania pomi�dzy atakami

#define SUPERATTACK_COST 60
#define ATTACK_COST		 10

#define STATE_SUPERATTACK	 18

#define STATE_SKILLUP		 19
#define STATE_SKILLRIGHT	 20
#define STATE_SKILLDOWN		 21
#define STATE_SKILLLEFT		 22

#define STATE_SKILLWALKUP	 23
#define STATE_SKILLWALKRIGHT 24
#define STATE_SKILLWALKDOWN	 25
#define STATE_SKILLWALKLEFT	 26

class Player: public GObject
{
public:
	Player();
	~Player();

	void DrawRect(int tex_id,float xo,float yo,float xf,float yf);
	void NextFrame(int max);  // max to ca�kowita liczba klatek w animacji

	int GetFrameTop();
	int GetFrameBottom();
	int GetFrameTexTiles();

	bool CollidesWithEnemy(std::list<GObject*> &enemies,int next_tile);
	bool CollidesUD(bool *collision_map,std::list<GObject*> &enemies,bool up);
	bool CollidesLR(bool *collision_map,std::list<GObject*> &enemies,bool right,bool vertical);

	void RestrictedMoveUp(bool *collision_map,std::list<GObject*> &enemies,int h1,/*int h2,*/int game_height);
	void RestrictedMoveDown(bool *collision_map,std::list<GObject*> &enemies,int h1,/*int h2,*/int game_height, int bottom_limit);
	void MoveUp(bool *collision_map,std::list<GObject*> &enemies);
	void MoveDown(bool *collision_map,std::list<GObject*> &enemies);
	void MoveRight(bool *collision_map,std::list<GObject*> &enemies,bool vertical);
	void MoveLeft(bool *collision_map,std::list<GObject*> &enemies,bool vertical);

	void SetMaxSP(int max_sp);
	int  GetMaxSP();
	void SetSP(int sp);
	int  GetSP();
	void SetCharge(int c);
	int  GetCharge();
	void SetMaxPoints(int max_points);
	void SetPoints(int points);
	int  GetMaxPoints();
	int	 GetPoints();
	void SetFrameAttackDelay(int fad);
	int  GetFrameAttackDelay();

	void StartSuperAttack();
	bool IsAttacking();
	bool IsSuperAttacking();
	bool IsDoingSkill();
	bool IsWalking();

	void TransformIntoSkillState();
	void Draw(int tex_id,int tex_w,int tex_h,bool run);

	
	void Stop();
	void StopDoingSkill();

	void SetCooldown(int c);
	int  GetCooldown();

	void SetLogicState(int input_state);
	int  GetLogicState();

protected:
	int changingStateSuperAttack;
private:
	int max_SP,SP,charge,cooldown,
		max_Points, pointss,
	    frame_walk_delay,
	    frame_attack_delay,
	    frame_texture_tiles, //liczba p�ytek tekstury zajmuj�cych bok ramki sprite'a
	    step_length; // TILE_SIZE musi by� wielokrotno�ci� step_length
	int logic_state;
};