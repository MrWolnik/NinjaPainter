#include "Overlay.h"
#include <string>

Overlay::Overlay()
{
	tiles_w = 6;
	tiles_h = 4;
}
Overlay::~Overlay(){}

void Overlay::SetX(int posx)
{
	x = posx;
}
int Overlay::GetX()
{
	return x;
}
void Overlay::SetY(int posy)
{
	y = posy;
}
int Overlay::GetY()
{
	return y;
}
int Overlay::GetWidth()
{
	return tiles_w*TILE_SIZE;
}
void Overlay::SetBarOffsetX(int ox)
{
	bar_offset_x = ox;
}

/* \brieffunkcja rysuje nak�adki*/
void Overlay::Draw(int tex_id)
{	
	int screen_x,screen_y;
	
	screen_x = x + SCENE_Xo;
	screen_y = y + SCENE_Yo;

	glEnable(GL_TEXTURE_2D);
	
	glBindTexture(GL_TEXTURE_2D,tex_id);
	glBegin(GL_QUADS);
		glTexCoord2f(0.0f,1.0f);	glVertex3i(screen_x					   ,screen_y					,-2);
		glTexCoord2f(1.0f,1.0f);	glVertex3i(screen_x + tiles_w*TILE_SIZE,screen_y					,-2);
		glTexCoord2f(1.0f,0.0f);	glVertex3i(screen_x + tiles_w*TILE_SIZE,screen_y + tiles_h*TILE_SIZE,-2);
		glTexCoord2f(0.0f,0.0f);	glVertex3i(screen_x					   ,screen_y + tiles_h*TILE_SIZE,-2);
	glEnd();

	glDisable(GL_TEXTURE_2D);
}
/*funkcja rysuje nak�adke z ilo�ci� �ycia i ilo�ci� sp */
void Overlay::DrawBars(int max1,int act1,int max2,int act2)
{
	int screen_x,screen_y;
	float factor1,factor2;
	int bar1_length,bar2_length;

	screen_x = x + SCENE_Xo;
	screen_y = y + SCENE_Yo;
	factor1 = ((float)act1/(float)max1);
	factor2 = ((float)act2/(float)max2);
	bar1_length = (tiles_w*TILE_SIZE - 2*TILE_SIZE - TILE_SIZE/2)*factor1;
	bar2_length = (tiles_w*TILE_SIZE - 2*TILE_SIZE - TILE_SIZE/2)*factor2;

	if(factor1 >= 0.5f) glColor3f( (1.0f-factor1)*2, 1.0f, 0.0f); //z zielonego na ��ty
	else glColor3f(1.0f, factor1*2, 0.0f); //z ��tego na czerwony
	glBegin(GL_QUADS);
		glVertex3i(screen_x + bar_offset_x			    ,screen_y + 5*(TILE_SIZE/8),-1);
		glVertex3i(screen_x + bar_offset_x + bar1_length,screen_y + 5*(TILE_SIZE/8),-1);
		glVertex3i(screen_x + bar_offset_x + bar1_length,screen_y + 7*(TILE_SIZE/8),-1);
		glVertex3i(screen_x + bar_offset_x			    ,screen_y + 7*(TILE_SIZE/8),-1);
	glEnd();

	glColor3f(0.2f, 0.5f, 1.0f);
	glBegin(GL_QUADS);
		glVertex3i(screen_x + bar_offset_x			    ,screen_y + 1*(TILE_SIZE/8),-1);
		glVertex3i(screen_x + bar_offset_x + bar2_length,screen_y + 1*(TILE_SIZE/8),-1);
		glVertex3i(screen_x + bar_offset_x + bar2_length,screen_y + 3*(TILE_SIZE/8),-1);
		glVertex3i(screen_x + bar_offset_x			    ,screen_y + 3*(TILE_SIZE/8),-1);
	glEnd();

	glColor4f(1, 1, 1, 1);//przywracamy kolor do bia�ego po narysowaniu pask�w (aby nie wp�ywa� na tekstury)
}

/*funkcja rysuje ekwipunek na erkranie oraz tekst z iloscia zdobytych artefaktow*/
void Overlay::drawArtefactAmmount(int * artefactAmmount, int arraySize) {
	int screen_x, screen_y;
	screen_x = x + SCENE_Xo;
	screen_y = y + SCENE_Yo;
	std::string result = "";
	for (int i = 0; i < arraySize; i++)
	{
		result += std::to_string(artefactAmmount[i])  + "  ";
	}
	int w = glutBitmapLength(GLUT_BITMAP_TIMES_ROMAN_24, (const unsigned char*)result.c_str());
	glRasterPos2f(screen_x+18, screen_y + tiles_h * TILE_SIZE - 90);
	int len = strlen(result.c_str());
	for (int i = 0; i < len; i++)
	{
		glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, result[i]);
	}
}

/*Funkcja wy�wietla zdobtyte punkty gracza*/
void Overlay::drawPoints(int points) {
	int screen_x, screen_y;
	screen_x = x + SCENE_Xo;
	screen_y = y + SCENE_Yo;
	std::string result = "Punkty Gracza: ";
	result += std::to_string(points);
	int w = glutBitmapLength(GLUT_BITMAP_TIMES_ROMAN_24, (const unsigned char*)result.c_str());
	glRasterPos2f(screen_x - tiles_w * TILE_SIZE*2, screen_y + tiles_h * TILE_SIZE*6+50);
	int len = strlen(result.c_str());
	for (int i = 0; i < len; i++)
	{
		glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, result[i]);
	}
}