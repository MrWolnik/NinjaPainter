#pragma once

#include "GObject.h"
#include "Player1.h"

class Enemy: public GObject
{
public:
	Enemy();
	virtual ~Enemy(); //gdy wywo�amy destruktor wroga, zostanie wykonany destruktor odpowiedniej podklasy

	void Draw(int tex_id,int tex_w,int tex_h,bool run);
	void DrawRect(int tex_id,float xo,float yo,float xf,float yf);
	void NextFrame(int max);

	int GetFrameTop();
	int GetFrameBottom();
	void SetFrameTexTiles(int ftt);

	void Move();

private:
	int frame_walk_delay,
		frame_attack_delay,
		frame_dying_delay,
		frame_texture_tiles, //numer p�ytek tekstury zajmuj�cych bok ramki sprite'a
		step_length; //TILE_SIZE musi by� wielokrotno�ci� step_length
};