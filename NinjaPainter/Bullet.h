#pragma once

#include "Scene.h"
#include "Player1.h"

#define STATE_LEVITATEUP		1
#define STATE_LEVITATERIGHT		2
#define STATE_LEVITATEDOWN		3
#define STATE_LEVITATELEFT		4

#define STATE_ENDINGUP			5
#define STATE_ENDINGRIGHT		6
#define STATE_ENDINGDOWN		7
#define STATE_ENDINGLEFT		8

class Bullet
{
public:
	Bullet();
	virtual ~Bullet();

	void SetPosition(int x,int y);
	int  GetY();
	int  GetX();
	void SetHitHeight(int hh);
	int  GetHitHeight();
	void SetHitWidth(int hw);
	int  GetHitWidth();
	void SetFrameTexTiles(int ftt);
	int  GetFrameTexTiles();
	Rect GetFrameBox();
	Rect GetHitBox();
	bool Intersection(Rect box1, Rect box2);

	bool CollidesObjUD(bool *proj_collision_map,bool up);
	bool CollidesObjLR(bool *proj_collision_map,bool right);

	void LevitateUp(bool *proj_collision_map);
	void LevitateDown(bool *proj_collision_map);
	void LevitateRight(bool *proj_collision_map);
	void LevitateLeft(bool *proj_collision_map);

	bool ComputeEnemyCollisions(std::list<GObject*> &enemies);
	bool Logic(bool *proj_collision_map,std::list<GObject*> &enemies);

	void DrawRect(int tex_id,float xo,float yo,float xf,float yf);
	virtual void Draw(int tex_id,int tex_w,int tex_h,bool run){};

	void SetState(int s);
	int  GetState();

	void NextFrame(int max);
	int  GetFrame();
	int  GetDelay();

	void SetDamage(int d);
	int  GetDamage();

	int  GetStepLength();

	void SetDisplaceUp(bool b);
	void SetDisplaceDown(bool b);
	bool IsEnding();
	
private:
	int x,y, //lewy dolny piksel hitboxa pocisku
		hit_w,hit_h, //wymiary w pikselach trafionej cz�ci klatki sprite'a, nie musz� by� podzielne przez TILE_SIZE, ale w powi�zanych sprite'ach hitbox musi by� w centrum/�rodku ka�dej klatki 
		state,
		seq,delay,
		damage,
		frame_delay,
		frame_texture_tiles,
		step_length,
		displace_up,displace_down;
};