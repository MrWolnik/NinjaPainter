#include "Globals.h"
#include "Game.h"
#include "Menu.h"
#include <iostream>
#include <fstream>
#include <string.h>

Game game;
Menu menu;


int res_x, res_y, pos_x, pos_y;
void welcome_display_callback();
void reshape_callback(int, int);
void keys(unsigned char, int, int);

void AppReshape(int w, int h) 
{
	game.Reshape(w,h);
}
void AppRender()
{
	game.Render();
}
void AppKeyboard(unsigned char key, int x, int y)
{
	game.ReadKeyboard(key,x,y,true);
}
void AppKeyboardUp(unsigned char key, int x, int y)
{
	game.ReadKeyboard(key,x,y,false);
}
void AppSpecialKeys(int key, int x, int y)
{
	game.ReadSpecialKeyboard(key,x,y,true);
}
void AppSpecialKeysUp(int key, int x, int y)
{
	game.ReadSpecialKeyboard(key,x,y,false);
}
void AppMouse(int button, int state, int x, int y)
{
	game.ReadMouse(button,state,x,y);
}
void AppIdle()
{
	if(!game.Loop()) exit(0);
}

void main(int argc, char** argv)
{
	//int res_x,res_y,pos_x,pos_y;

	//Inicjalizacja GLUT
	glutInit(&argc, argv);

	//RGBA with double buffer
	glutInitDisplayMode(GLUT_RGBA | GLUT_ALPHA | GLUT_DOUBLE | GLUT_DEPTH);

	//Create centered window
	res_x = glutGet(GLUT_SCREEN_WIDTH);
	res_y = glutGet(GLUT_SCREEN_HEIGHT);
	pos_x = (res_x >> 1)-(GAME_WIDTH>>1); // dzielenie przez 2 (za pomoc� przesuni�cia bitowego, aby wy�rodkowa� okno)
	pos_y = (res_y >> 1)-(GAME_HEIGHT>>1);
	
	glutInitWindowPosition(pos_x,pos_y);
	glutInitWindowSize(GAME_WIDTH, GAME_HEIGHT);
	glutCreateWindow("Magnaci i Czarodzieje");

	//glutgameModeString("800x600:32");
	//glutEntergameMode();

	//Spowoduj, �e domy�lny kursor zniknie
	//glutSetCursor(GLUT_CURSOR_NONE);

	//Register callback functions
	glutReshapeFunc(reshape_callback);
	glutDisplayFunc(welcome_display_callback);
	glutKeyboardFunc(keys);
	glutKeyboardUpFunc(AppKeyboardUp);
	glutSpecialFunc(AppSpecialKeys);
	glutSpecialUpFunc(AppSpecialKeysUp);
	glutMouseFunc(AppMouse);
	//glutIdleFunc(AppIdle);

	//inicjalizacja glew
	GLint GlewInitResult = glewInit();
	if (GLEW_OK != GlewInitResult) 
	{
		printf("ERROR: %s\n",glewGetErrorString(GlewInitResult));
		exit(EXIT_FAILURE);
	}

	//inicjalizacja gry
	game.Init(1);

	//P�tla aplikacji
	glutMainLoop();	
}

/*funkcja tworzy menu g��wne*/
void welcome_display_callback() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glColor3f(2, 7, 2);
	glRasterPos3f(10, 35, 0);
	char msg1[] = "Czarodzieje i magnaci!";
	for (int i = 0; i < strlen(msg1); i++) {
		glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, msg1[i]);

	}

	glColor3f(2, 7, 2);
	glRasterPos3f(15, 17.5, 0);
	char msg2[] = "Wyjscie! (Q)";
	for (int i = 0; i < strlen(msg2); i++) {
		glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, msg2[i]);

	}

	glBegin(GL_POLYGON);
	glColor3ub(0, 150, 254);
	glVertex2f(35.0, 20.0);
	glVertex2f(10.0, 20.0);
	glVertex2f(10.0, 15.0);
	glVertex2f(35.0, 15.0);
	glEnd();

	glColor3f(2, 7, 2);
	glRasterPos3f(15, 25, 0);
	char msg3[] = "Ostatni zapis!(P)";
	for (int i = 0; i < strlen(msg3); i++) {
		glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, msg3[i]);

	}

	glBegin(GL_POLYGON);
	glColor3ub(0, 154, 254);
	glVertex2f(35.0, 27.0);
	glVertex2f(10.0, 27.0);
	glVertex2f(10.0, 22.0);
	glVertex2f(35.0, 22.0);
	glEnd();

	glColor3f(2, 7, 2);
	glRasterPos3f(15, 32, 0);
	char msg4[] = "Nowa Gra!(X)";
	for (int i = 0; i < strlen(msg4); i++) {
		glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, msg4[i]);

	}
	
	glBegin(GL_POLYGON);
	glColor3ub(0, 154, 254);
	glVertex2f(35.0, 29.0);
	glVertex2f(10.0, 29.0);
	glVertex2f(10.0, 34.0);
	glVertex2f(35.0, 34.0);
	glEnd();


	//background
	//glEnable(GL_TEXTURE_2D);



	//glBindTexture(GL_TEXTURE_2D,ts);

	glBegin(GL_POLYGON);
	glColor3ub(12, 2, 254);
	/*glTexCoord2f(0.0f, 1.0f);*/ glVertex2f(0.0, 0.0);
	/*glTexCoord2f(0.0f, 1.0f);*/ glVertex2f(40.0, 0.0);
	/*glTexCoord2f(0.0f, 1.0f);*/ glVertex2f(40.0, 40.0);
	/*glTexCoord2f(0.0f, 1.0f);*/ glVertex2f(0.0, 40.0);
	glEnd();
	
	glFlush();

	//game.Init(1);
	glutSwapBuffers();
}

void reshape_callback(int w, int h) {
	glViewport(0, 0, (GLsizei)w, (GLsizei)h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, 40.0, 0.0, 40.0, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);

}

/* \brief obs�uga przyciskow w menu*/
void keys(unsigned char key, int x, int y) {
	if (key == 'x') {
		glutDisplayFunc(AppRender);
		glutReshapeFunc(AppReshape);
		glutKeyboardFunc(AppKeyboard);
		glutIdleFunc(AppIdle);
		game.skipMusicInitialization = true;
		game.Init(1);
	}

	if (key == 'q') {
		exit(0);
	}

	if (key == 'p') {
		
		std::string dataLevel;
		std::string artefactC;
		int tempArt;
		int artefacts[5];
		std::fstream levelFile;
		levelFile.open("level.txt");
		
		std::fstream artefactFile;
		artefactFile.open("artefact.txt");

		for (int i = 0; i < 5; i++) {
			artefactFile >> artefacts[i];
		}
		int points;
		std::string points_s;
		std::fstream pointsFile;
		pointsFile.open("points.txt");
		while (getline(pointsFile, points_s)) {
			points = std::stoi(points_s);
		}


		game.setTempData(artefacts,points);

		while (getline(levelFile, dataLevel)) {
			std::cout << dataLevel;
		}

		int integerLevel = std::stoi(dataLevel);
		glutDisplayFunc(AppRender);
		glutReshapeFunc(AppReshape);
		glutKeyboardFunc(AppKeyboard);
		glutIdleFunc(AppIdle);
		game.skipMusicInitialization = true;
		game.Init(integerLevel);
	}
	glutPostRedisplay();
}




