#pragma once

#include "Player.h"
#include "Fireball.h"
class Player2: public Player
{
public:
	Player2();
	~Player2();

	bool Attack(std::list<GObject*> &enemies,std::list<Bullet*> &projectiles,bool *proj_collision_map);

private:
};