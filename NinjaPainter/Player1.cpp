#include "Player1.h"

Player1::Player1()
{
	SetMaxHealth(150);
	SetHealth(150);
	SetMaxSP(150);
	SetSP(150);
	SetCharge(0);
	SetDamage(10);
	SetMaxPoints(100000);
	SetPoints(0);
	SetLogicState(STATE_LOOKDOWN);
	changingStateSuperAttack = 7;
}
Player1::~Player1(){}

/* \brief funkcja atatku wojowiczki*/
bool Player1::Attack(GObject* enemy)
{
	bool hit = false;
	if(GetState() != STATE_SUPERATTACK && GetFrame() == 3 && GetDelay() == 0)
	{
		if(GetState() == STATE_ATTACKUP)
		{
			Rect attack_area;
			attack_area.top = GetY()+GetBaseHeight()+TILE_SIZE;
			attack_area.bottom = GetY()+GetBaseHeight();
			attack_area.left = GetX()-TILE_SIZE;
			attack_area.right = GetX()+GetBaseWidth()+TILE_SIZE;

			if( enemy->GetState() != STATE_DYING && Intersection(attack_area,enemy->GetHitBox()) ) 
			{
				int new_health = enemy->GetHealth() - GetDamage();
				if(new_health <= 0) enemy->SetState(STATE_DYING);
				else enemy->SetState(STATE_DAMAGEDOWN);
				if(new_health < 0) new_health = 0;
				enemy->SetHealth(new_health);
				enemy->SetFrame(0);
				enemy->SetDelay(0);
				hit = true;
			}
		}
		else if(GetState() == STATE_ATTACKDOWN)
		{
			Rect attack_area;
			attack_area.top = GetY();
			attack_area.bottom = GetY()-TILE_SIZE/2;
			attack_area.left = GetX()-TILE_SIZE;
			attack_area.right = GetX()+GetBaseWidth()+TILE_SIZE;

			if( enemy->GetState() != STATE_DYING && Intersection(attack_area,enemy->GetHitBox()) ) 
			{
				int new_health = enemy->GetHealth() - GetDamage();
				if(new_health <= 0) enemy->SetState(STATE_DYING);
				else enemy->SetState(STATE_DAMAGEUP);
				if(new_health < 0) new_health = 0;
				enemy->SetHealth(new_health);
				enemy->SetFrame(0);
				enemy->SetDelay(0);
				hit = true;
			}
		}
		else if(GetState() == STATE_ATTACKLEFT)
		{
			Rect attack_area;
			attack_area.top = GetY()+GetBaseHeight();
			attack_area.bottom = GetY();
			attack_area.left = GetX()-(TILE_SIZE/5)*7;
			attack_area.right = GetX();

			if( enemy->GetState() != STATE_DYING && Intersection(attack_area,enemy->GetHitBox()) ) 
			{
				int new_health = enemy->GetHealth() - GetDamage();
				if(new_health <= 0) enemy->SetState(STATE_DYING);
				else enemy->SetState(STATE_DAMAGERIGHT);
				if(new_health < 0) new_health = 0;
				enemy->SetHealth(new_health);
				enemy->SetFrame(0);
				enemy->SetDelay(0);
				hit = true;
			}
		}
		else if(GetState() == STATE_ATTACKRIGHT)
		{
			Rect attack_area;
			attack_area.top = GetY()+GetBaseHeight();
			attack_area.bottom = GetY();
			attack_area.left = GetX()+GetBaseWidth();
			attack_area.right = GetX()+GetBaseWidth()+(TILE_SIZE/5)*7;

			if( enemy->GetState() != STATE_DYING && Intersection(attack_area,enemy->GetHitBox()) ) 
			{
				int new_health = enemy->GetHealth() - GetDamage();
				if(new_health <= 0) enemy->SetState(STATE_DYING);
				else enemy->SetState(STATE_DAMAGELEFT);
				if(new_health < 0) new_health = 0;
				enemy->SetHealth(new_health);
				enemy->SetFrame(0);
				enemy->SetDelay(0);
				hit = true;
			}
		}	
	}
	else if(GetState() == STATE_SUPERATTACK && (GetFrame() > 0 || GetFrame() < 5) && GetDelay() == 0)
	{
		if(GetFrame() == 1)
		{
			Rect attack_area;
			attack_area.top = GetY()+GetBaseHeight()+TILE_SIZE*2;
			attack_area.bottom = GetY();
			attack_area.left = GetX()-TILE_SIZE*2;
			attack_area.right = GetX()+GetBaseWidth();

			if( enemy->GetState() != STATE_DYING && Intersection(attack_area,enemy->GetHitBox()) ) 
			{
				int new_health = enemy->GetHealth() - GetDamage();
				if(new_health <= 0) enemy->SetState(STATE_DYING);
				else enemy->SetState(STATE_DAMAGEDOWN);
				if(new_health < 0) new_health = 0;
				enemy->SetHealth(new_health);
				enemy->SetFrame(0);
				enemy->SetDelay(0);
				hit = true;
			}
		}
		else if(GetFrame() == 2)
		{
			Rect attack_area;
			attack_area.top = GetY()+GetBaseHeight();
			attack_area.bottom = GetY()-TILE_SIZE;
			attack_area.left = GetX()-TILE_SIZE*2;
			attack_area.right = GetX()+GetBaseWidth();

			if( enemy->GetState() != STATE_DYING && Intersection(attack_area,enemy->GetHitBox()) ) 
			{
				int new_health = enemy->GetHealth() - GetDamage();
				if(new_health <= 0) enemy->SetState(STATE_DYING);
				else enemy->SetState(STATE_DAMAGERIGHT);
				if(new_health < 0) new_health = 0;
				enemy->SetHealth(new_health);
				enemy->SetFrame(0);
				enemy->SetDelay(0);
				hit = true;
			}
		}
		else if(GetFrame() == 3)
		{
			Rect attack_area;
			attack_area.top = GetY()+GetBaseHeight();
			attack_area.bottom = GetY()-TILE_SIZE;
			attack_area.left = GetX();
			attack_area.right = GetX()+GetBaseWidth()+TILE_SIZE*2;

			if( enemy->GetState() != STATE_DYING && Intersection(attack_area,enemy->GetHitBox()) ) 
			{
				int new_health = enemy->GetHealth() - GetDamage();
				if(new_health <= 0) enemy->SetState(STATE_DYING);
				else enemy->SetState(STATE_DAMAGEUP);
				if(new_health < 0) new_health = 0;
				enemy->SetHealth(new_health);
				enemy->SetFrame(0);
				enemy->SetDelay(0);
				hit = true;
			}
		}
		else if(GetFrame() == 4)
		{
			Rect attack_area;
			attack_area.top = GetY()+GetBaseHeight()+TILE_SIZE*2;
			attack_area.bottom = GetY();
			attack_area.left = GetX();
			attack_area.right = GetX()+GetBaseWidth()+TILE_SIZE*2;

			if( enemy->GetState() != STATE_DYING && Intersection(attack_area,enemy->GetHitBox()) ) 
			{
				int new_health = enemy->GetHealth() - GetDamage();
				if(new_health <= 0) enemy->SetState(STATE_DYING);
				else enemy->SetState(STATE_DAMAGELEFT);
				if(new_health < 0) new_health = 0;
				enemy->SetHealth(new_health);
				enemy->SetFrame(0);
				enemy->SetDelay(0);
				hit = true;
			}
		}
	}
	return hit;
}

void Player1::SetShieldState(int input_state)
{
	// je�li input_state by� stanem umiej�tno�ci w stopie, musimy go przekszta�ci� w stan ruchu w przypadku poruszania si� klawiszami, poniewa� input_state jest utrzymywany podczas wykonywania umiej�tno�ci. Gdyby�my stali w miejscu, funkcja Stop skorygowa�aby ten stan
	if(input_state == STATE_SKILLUP   || input_state == STATE_SKILLDOWN ||
	   input_state == STATE_SKILLLEFT || input_state == STATE_SKILLRIGHT )
	{
		if(input_state == STATE_SKILLUP) SetState(STATE_SKILLWALKUP);
		else if(input_state == STATE_SKILLDOWN) SetState(STATE_SKILLWALKDOWN);
		else if(input_state == STATE_SKILLLEFT) SetState(STATE_SKILLWALKLEFT);
		else if(input_state == STATE_SKILLRIGHT) SetState(STATE_SKILLWALKRIGHT);
	}
	else if(input_state == STATE_SKILLWALKUP   || input_state == STATE_SKILLWALKDOWN || 
			input_state == STATE_SKILLWALKLEFT || input_state == STATE_SKILLWALKRIGHT )
	{
		SetState(input_state);
	}
	// je�li input_state nie by� umiej�tno�ci�, przekszta�camy go w umiej�tno�� 
	else TransformIntoSkillState();
}

void Player1::TestShieldProtection(int attack_state,int damage,int x,int y)
{
	bool shield_useful = false;
	int damage_state;

	if(attack_state == STATE_ATTACKUP)
	{
		if( (GetState() == STATE_SKILLDOWN)                                                        || 
			(GetState() == STATE_SKILLWALKDOWN)                                                    || 
		    ((GetState() == STATE_SKILLLEFT || GetState() == STATE_SKILLWALKLEFT) &&  x < GetX())  ||
			((GetState() == STATE_SKILLRIGHT || GetState() == STATE_SKILLWALKRIGHT) &&  x > GetX()) ) shield_useful = true;
		damage_state = STATE_DAMAGEDOWN;
	}
	else if(attack_state == STATE_ATTACKDOWN)
	{
		if( (GetState() == STATE_SKILLUP)                                                          || 
			(GetState() == STATE_SKILLWALKUP)                                                      || 
		    ((GetState() == STATE_SKILLLEFT || GetState() == STATE_SKILLWALKLEFT) &&  x < GetX())  ||
			((GetState() == STATE_SKILLRIGHT || GetState() == STATE_SKILLWALKRIGHT) &&  x > GetX()) ) shield_useful = true;
		damage_state = STATE_DAMAGEUP;
	}
	else if(attack_state == STATE_ATTACKLEFT)
	{
		if( (GetState() == STATE_SKILLRIGHT)                                                       || 
			(GetState() == STATE_SKILLWALKRIGHT)                                                   || 
		    ((GetState() == STATE_SKILLDOWN || GetState() == STATE_SKILLWALKDOWN) &&  y < GetY())  ||
			((GetState() == STATE_SKILLUP || GetState() == STATE_SKILLWALKUP) &&  y > GetY())       ) shield_useful = true;
		damage_state = STATE_DAMAGERIGHT;
	}
	else if(attack_state == STATE_ATTACKRIGHT)
	{
		if( (GetState() == STATE_SKILLLEFT)                                                        || 
			(GetState() == STATE_SKILLWALKLEFT)                                                    || 
		    ((GetState() == STATE_SKILLDOWN || GetState() == STATE_SKILLWALKDOWN) &&  y < GetY())  ||
			((GetState() == STATE_SKILLUP || GetState() == STATE_SKILLWALKUP) &&  y > GetY())       ) shield_useful = true;
		damage_state = STATE_DAMAGELEFT;
	}
	
	if(shield_useful)
	{
		int new_sp = GetSP() - damage;
		if(new_sp >= 0) SetSP(new_sp);
		else
		{
			SetSP(0);
			int new_health = GetHealth() + new_sp;
			if(new_health <= 0)
			{
				SetHealth(0);
				SetState(STATE_DYING);
			}
			else
			{
				SetHealth(new_health);
				SetState(damage_state);
			}
			SetFrame(0);
			SetDelay(0);
		}
	}
	else
	{
		int new_health = GetHealth() - damage;
		if(new_health <= 0)
		{
			SetHealth(0);
			SetState(STATE_DYING);
		}
		else
		{
			SetHealth(new_health);
			SetState(damage_state);
		}
		SetFrame(0);
		SetDelay(0);
	}
}