#pragma once

#include "Enemy.h"

class Monster1: public Enemy
{
public:
	Monster1();
	~Monster1();

	bool Attack(GObject* player);
	bool IA(bool *collision_map,std::list<GObject*> &enemies,GObject* p1/*,GObject* p2*/);
};