#include "Player2.h"

Player2::Player2()
{
	SetMaxHealth(100);
	SetHealth(100);
	SetMaxSP(150);
	SetSP(150);
	SetCharge(0);
	SetLogicState(STATE_LOOKDOWN);
	SetMaxPoints(100000);
	SetPoints(0);
	SetDamage(0);
	changingStateSuperAttack = 12;
}
Player2::~Player2(){}

/* \brief funkcja ataku czarodziejki ognista kula*/
bool Player2::Attack(std::list<GObject*> &enemies,std::list<Bullet*> &projectiles,bool *proj_collision_map)
{
	int x,y;
	//offset jest u�ywany do zmiany po�o�enia pocisku, tak aby�my musieli sprawdza� kolizj� tylko na przeci�ciu kafelk�w (niezale�nie od pozycji gracza i kroku pocisku)
	int offset_x, offset_y;

	Fireball aux;
	int proyectile_step = aux.GetStepLength();

	offset_x = GetX()%proyectile_step;
	offset_y = GetY()%proyectile_step;

	if(GetState() != STATE_SUPERATTACK && GetFrame() == 2 && GetDelay() == 0)
	{
		if(GetState() == STATE_ATTACKUP)
		{
			projectiles.push_back(new Fireball);
			x = GetX() + (GetBaseWidth() - projectiles.back()->GetHitWidth())/2;
			y = GetY() + GetBaseHeight() - offset_y;
			projectiles.back()->SetState(STATE_LEVITATEUP);
			projectiles.back()->SetPosition(x,y);
			//ocenia, czy pocisk pojawia si� na przeciwniku lub obiekcie
			if( projectiles.back()->ComputeEnemyCollisions(enemies) || projectiles.back()->CollidesObjUD(proj_collision_map,false) || projectiles.back()->CollidesObjUD(proj_collision_map,true))
				projectiles.back()->SetState(STATE_ENDINGUP);
		}
		else if(GetState() == STATE_ATTACKDOWN)
		{
			projectiles.push_back(new Fireball);
			x = GetX() + (GetBaseWidth() - projectiles.back()->GetHitWidth())/2;
			y = GetY() - projectiles.back()->GetHitHeight() + (proyectile_step - offset_y)%proyectile_step;
			projectiles.back()->SetState(STATE_LEVITATEDOWN);
			projectiles.back()->SetPosition(x,y);
			//ocenia, czy pocisk pojawia si� na przeciwniku lub obiekcie
			if( projectiles.back()->ComputeEnemyCollisions(enemies) || projectiles.back()->CollidesObjUD(proj_collision_map,false) )
				projectiles.back()->SetState(STATE_ENDINGDOWN);
		}
		else if(GetState() == STATE_ATTACKLEFT)
		{
			projectiles.push_back(new Fireball);
			x = GetX() - projectiles.back()->GetHitWidth() + (proyectile_step - offset_x)%proyectile_step;
			y = GetY() + (GetBaseHeight() - projectiles.back()->GetHitHeight())/2;
			projectiles.back()->SetState(STATE_LEVITATELEFT);
			projectiles.back()->SetPosition(x,y);
			//ocenia, czy pocisk pojawia si� na przeciwniku lub obiekcie
			if( projectiles.back()->ComputeEnemyCollisions(enemies) || projectiles.back()->CollidesObjLR(proj_collision_map,false) )
				projectiles.back()->SetState(STATE_ENDINGLEFT);
		}
		else if(GetState() == STATE_ATTACKRIGHT)
		{
			projectiles.push_back(new Fireball);
			x = GetX() + GetBaseWidth() - offset_x;
			y = GetY() + (GetBaseHeight() - projectiles.back()->GetHitHeight())/2;
			projectiles.back()->SetState(STATE_LEVITATERIGHT);
			projectiles.back()->SetPosition(x,y);
			//ocenia, czy pocisk pojawia si� na przeciwniku lub obiekcie
			if( projectiles.back()->ComputeEnemyCollisions(enemies) || projectiles.back()->CollidesObjLR(proj_collision_map,false) || projectiles.back()->CollidesObjLR(proj_collision_map,true))
				projectiles.back()->SetState(STATE_ENDINGRIGHT);
		}
		SetSP(GetSP() - ATTACK_COST);
		return true;
	}
	else if(GetState() == STATE_SUPERATTACK && GetDelay() == 0 && GetSP() >= ATTACK_COST/2)
	{
		if(GetFrame() == 3)
		{
			projectiles.push_back(new Fireball);
			x = GetX() + (GetBaseWidth() - projectiles.back()->GetHitWidth())/2;
			y = TILE_SIZE + GetY() + GetBaseHeight() - offset_y;
			projectiles.back()->SetState(STATE_LEVITATEUP);
			projectiles.back()->SetPosition(x,y);
			//ocenia, czy pocisk pojawia si� na przeciwniku lub obiekcie
			if( projectiles.back()->ComputeEnemyCollisions(enemies) || projectiles.back()->CollidesObjUD(proj_collision_map,false) || projectiles.back()->CollidesObjUD(proj_collision_map,true))
				projectiles.back()->SetState(STATE_ENDINGUP);
			SetSP(GetSP() - ATTACK_COST/2);
			return true;
		}
		else if(GetFrame() == 4)
		{
			projectiles.push_back(new Fireball);
			x = GetX() + GetBaseWidth() - offset_x;
			y = TILE_SIZE + GetY() + (GetBaseHeight() - projectiles.back()->GetHitHeight())/2;
			projectiles.back()->SetState(STATE_LEVITATERIGHT);
			projectiles.back()->SetPosition(x,y);
			//ocenia, czy pocisk pojawia si� na przeciwniku lub obiekcie
			if( projectiles.back()->ComputeEnemyCollisions(enemies) || projectiles.back()->CollidesObjLR(proj_collision_map,false) || projectiles.back()->CollidesObjLR(proj_collision_map,true))
				projectiles.back()->SetState(STATE_ENDINGRIGHT);
			projectiles.back()->SetDisplaceUp(true);
			SetSP(GetSP() - ATTACK_COST/2);
			return true;
		}
		else if(GetFrame() == 5)
		{
			projectiles.push_back(new Fireball);
			x = GetX() + GetBaseWidth() - offset_x;
			y = TILE_SIZE + GetY() + (GetBaseHeight() - projectiles.back()->GetHitHeight())/2;
			projectiles.back()->SetState(STATE_LEVITATERIGHT);
			projectiles.back()->SetPosition(x,y);
			//ocenia, czy pocisk pojawia si� na przeciwniku lub obiekcie
			if( projectiles.back()->ComputeEnemyCollisions(enemies) || projectiles.back()->CollidesObjLR(proj_collision_map,false) || projectiles.back()->CollidesObjLR(proj_collision_map,true))
				projectiles.back()->SetState(STATE_ENDINGRIGHT);
			SetSP(GetSP() - ATTACK_COST/2);
			return true;
		}
		else if(GetFrame() == 6)
		{
			projectiles.push_back(new Fireball);
			x = GetX() + GetBaseWidth() - offset_x;
			y = TILE_SIZE + GetY() + (GetBaseHeight() - projectiles.back()->GetHitHeight())/2;
			projectiles.back()->SetState(STATE_LEVITATERIGHT);
			projectiles.back()->SetPosition(x,y);
			//ocenia, czy pocisk pojawia si� na przeciwniku lub obiekcie
			if( projectiles.back()->ComputeEnemyCollisions(enemies) || projectiles.back()->CollidesObjLR(proj_collision_map,false) || projectiles.back()->CollidesObjLR(proj_collision_map,true))
				projectiles.back()->SetState(STATE_ENDINGRIGHT);
			projectiles.back()->SetDisplaceDown(true);
			SetSP(GetSP() - ATTACK_COST/2);
			return true;
		}
		else if(GetFrame() == 7)
		{
			projectiles.push_back(new Fireball);
			x = GetX() + (GetBaseWidth() - projectiles.back()->GetHitWidth())/2;
			y = TILE_SIZE + GetY() - projectiles.back()->GetHitHeight() + (proyectile_step - offset_y)%proyectile_step;
			projectiles.back()->SetState(STATE_LEVITATEDOWN);
			projectiles.back()->SetPosition(x,y);
			//ocenia, czy pocisk pojawia si� na przeciwniku lub obiekcie
			if( projectiles.back()->ComputeEnemyCollisions(enemies) || projectiles.back()->CollidesObjUD(proj_collision_map,false) )
				projectiles.back()->SetState(STATE_ENDINGDOWN);
			SetSP(GetSP() - ATTACK_COST/2);
			return true;
		}
		else if(GetFrame() == 8)
		{
			projectiles.push_back(new Fireball);
			x = GetX() - projectiles.back()->GetHitWidth() + (proyectile_step - offset_x)%proyectile_step;
			y = TILE_SIZE + GetY() + (GetBaseHeight() - projectiles.back()->GetHitHeight())/2;
			projectiles.back()->SetState(STATE_LEVITATELEFT);
			projectiles.back()->SetPosition(x,y);
			//ocenia, czy pocisk pojawia si� na przeciwniku lub obiekcie
			if( projectiles.back()->ComputeEnemyCollisions(enemies) || projectiles.back()->CollidesObjLR(proj_collision_map,false) )
				projectiles.back()->SetState(STATE_ENDINGLEFT);
			projectiles.back()->SetDisplaceDown(true);
			SetSP(GetSP() - ATTACK_COST/2);
			return true;
		}
		else if(GetFrame() == 9)
		{
			projectiles.push_back(new Fireball);
			x = GetX() - projectiles.back()->GetHitWidth() + (proyectile_step - offset_x)%proyectile_step;
			y = TILE_SIZE + GetY() + (GetBaseHeight() - projectiles.back()->GetHitHeight())/2;
			projectiles.back()->SetState(STATE_LEVITATELEFT);
			projectiles.back()->SetPosition(x,y);
			//ocenia, czy pocisk pojawia si� na przeciwniku lub obiekcie
			if( projectiles.back()->ComputeEnemyCollisions(enemies) || projectiles.back()->CollidesObjLR(proj_collision_map,false) )
				projectiles.back()->SetState(STATE_ENDINGLEFT);
			SetSP(GetSP() - ATTACK_COST/2);
			return true;
		}
		else if(GetFrame() == 10)
		{
			projectiles.push_back(new Fireball);
			x = GetX() - projectiles.back()->GetHitWidth() + (proyectile_step - offset_x)%proyectile_step;
			y = TILE_SIZE + GetY() + (GetBaseHeight() - projectiles.back()->GetHitHeight())/2;
			projectiles.back()->SetState(STATE_LEVITATELEFT);
			projectiles.back()->SetPosition(x,y);
			//ocenia, czy pocisk pojawia si� na przeciwniku lub obiekcie
			if( projectiles.back()->ComputeEnemyCollisions(enemies) || projectiles.back()->CollidesObjLR(proj_collision_map,false) )
				projectiles.back()->SetState(STATE_ENDINGLEFT);
			projectiles.back()->SetDisplaceUp(true);
			SetSP(GetSP() - ATTACK_COST/2);
			return true;
		}
		else if(GetFrame() == 11)
		{
			projectiles.push_back(new Fireball);
			x = GetX() + (GetBaseWidth() - projectiles.back()->GetHitWidth())/2;
			y = TILE_SIZE + GetY() + GetBaseHeight() - offset_y;
			projectiles.back()->SetState(STATE_LEVITATEUP);
			projectiles.back()->SetPosition(x,y);
			//ocenia, czy pocisk pojawia si� na przeciwniku lub obiekcie
			if( projectiles.back()->ComputeEnemyCollisions(enemies) || projectiles.back()->CollidesObjUD(proj_collision_map,false) || projectiles.back()->CollidesObjUD(proj_collision_map,true))
				projectiles.back()->SetState(STATE_ENDINGUP);
			SetSP(GetSP() - ATTACK_COST/2);
			return true;
		}
	}
	return false;
}