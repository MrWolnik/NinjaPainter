#pragma once

#include "Scene.h"
#include "Player.h"

class Token
{
public:
	Token();
	virtual ~Token();

	void  SetTile(int tx,int ty);
	int   GetX();
	int   GetY();
	Rect GetHitBox();

	void  Draw(int tex_id,int tex_w,int tex_h,bool run);
	void  NextFrame(int max);
	int   GetFrame();

private:
	int x,y,
		seq,delay,
		frame_delay,
		height_overground;
};

class cHeart: public Token
{
public:
	/*funkja Effect - po zebraniu serca naliczane jest �ycie*/
	bool Effect(Player &player)
	{
		if(player.GetHealth() == player.GetMaxHealth()) return false;
		int new_health = player.GetHealth() + 40;
		if(new_health > player.GetMaxHealth()) player.SetHealth(player.GetMaxHealth());
		else player.SetHealth(new_health);
		return true;
	}
};

class Artefact1 : public Token
{
public:
	bool Effect(Player& player)
	{
		return true;
	}
};

class Artefact2 : public Token
{
public:
	/*funkja Effect - po zebraniu serca dodawane jest �ycie*/
	bool Effect(Player& player)
	{
		if (player.GetHealth() == player.GetMaxHealth()) return false;
		int new_health = 150;
		if (new_health > player.GetMaxHealth()) player.SetHealth(player.GetMaxHealth());
		else player.SetHealth(new_health);
		return true;
	}
};
class Artefact3 : public Token
{
public:
	/*funkja Effect - po zebraniu artefaktu naliczane jest s� punkty*/
	bool Effect(Player& player)
	{
		if (player.GetPoints() == player.GetMaxPoints()) return false;
		int new_points = player.GetPoints() + 1000;
		if (new_points > player.GetMaxPoints()) player.SetPoints(player.GetMaxPoints());
		else player.SetPoints(new_points);
		return true;
	}
};

class Artefact4 : public Token
{
public:
	/*funkja Effect - po zebraniu artefaktu naliczane jest s� punkty*/
	bool Effect(Player& player)
	{
		if (player.GetPoints() == player.GetMaxPoints()) return false;
		int new_points = player.GetPoints() + 1000;
		if (new_points > player.GetMaxPoints()) player.SetPoints(player.GetMaxPoints());
		else player.SetPoints(new_points);
		return true;
	}
};
class Artefact5 : public Token
{
public:
	bool Effect(Player& player)
	{
		return true;
	}
};