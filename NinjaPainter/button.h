#pragma once


class Button {
public:
    Button(double top, double left, double width, double height, void(*f)(void));
    ~Button();
    void CheckClick(double x, double y);
private:
    double w;
    double h;
    double t;
    double l;
    double b;
    void* onClick;
};