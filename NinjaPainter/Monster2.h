#pragma once

#include "Enemy.h"
#include "Arrow.h"
class Monster2: public Enemy
{
public:
	Monster2();
	~Monster2();

	bool Attack(std::list<GObject*> &players,std::list<Bullet*> &projectiles,bool *proj_collision_map);
	bool IA(bool *collision_map,bool *proj_collision_map,std::list<GObject*> &enemies,GObject* p1/*,GObject* p2)*/);
};